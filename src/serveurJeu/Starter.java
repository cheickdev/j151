package serveurJeu;

import java.io.IOException;

public class Starter {

	/**
	 * @param args
	 * @throws IOException 
	 */
	Thread ts;
	ServeurJeu serveurJeu;
	int port;
	public Starter() throws IOException {
		port = 7000;
		serveurJeu = new ServeurJeu(7000);
		ts = new Thread(serveurJeu);
	}
	
	public Starter(int port) throws IOException {
		this.port = port;
		serveurJeu = new ServeurJeu(port);
		ts = new Thread(serveurJeu);
	}
	
	public static void main(String[] args) throws IOException {
		Starter st = new Starter();
		st.start();
	}

	public void start(){
		ts.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop(){
		ts.stop();
	}

	public int getPort() {
		return port;
	}
	
	public void setPort(int port){
		this.port = port;
	}
}
