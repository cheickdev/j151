/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serveurJeu;

import commun.Card;
import java.util.LinkedList;

/**
 *
 * @author Mcicheick
 */
public class Deck extends LinkedList<Card> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String[] couleurs = new String[]{"pique", "coeur","trefle","carreau"};
    public Deck() {
        for(int j = 1; j <= 13; j++){
            for(int i = 0; i < 4; i++){
                String num = "";
                if(j==11) num = "V";
                else if(j==12) num = "D";
                else if(j==13) num = "R";
                else num = ""+j;
                if(j < 2 || j > 6)add(new Card(num, couleurs[i]));
            }
        }
    }
    
    Deck(LinkedList<Card> cl){
        addAll(cl);
    }
    
    Card draw(Deck d){
        return d.poll();
    }
    
    int cut (int n) { 
	int res=0;
	for (int i=0; i<n; i++)
	    if (Math.random() < 0.5)
		res++;
	return res;
    }
    
    LinkedList<Card> split (int c){
        LinkedList<Card> cards = new LinkedList<Card>();
        for(int i = 0; i < c; i++){
            cards.add(poll());
        }
        return cards;
    }
    
    LinkedList<Card> riffle (LinkedList<Card> l2){
        double k1 = size();
        double k2 = l2.size();
        double p1 = k1/(k1+k2);
        LinkedList<Card> cards = new LinkedList<Card>();
        for(;!(isEmpty() && l2.isEmpty());){
            k1 = size();
            k2 = l2.size();
            p1 = k1/(k1+k2);
            if(Math.random() < p1) cards.add(poll());
            else cards.add(l2.poll());
        }
        return cards;
    }
    
    void riffleShuffle(int n){
        for(int i = 0; i < n; i++) {
            addAll(riffle(split(cut(size()))));
        }
    }
    
    @Override
    public String toString() {
        if(isEmpty()) return "[]";
        String s = "";
        for(Card card: this){
            if(!card.equals(getLast())) s = s+card+", ";
        }
        s = s+getLast();
        return s;
    }
    
    public static void main(String[] args){
        Deck d = new Deck();
        System.out.println(d);
        d.riffleShuffle(10);
        System.out.println(d);
    }
      
}