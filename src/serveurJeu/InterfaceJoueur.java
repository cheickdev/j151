/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 13 juin 2011
 */

package serveurJeu;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;


import commun.Card;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public interface InterfaceJoueur
{
    /**
     * @param id 
     * @param cartes
     * @throws IOException 
     */
    public void aDeposer(int id, Card[] cartes) throws IOException;
    
    /**
     * @param i
     * @throws IOException 
     */
    public void aGagner(int i) throws IOException;
    
    /**
     * @param nom 
     * @throws IOException 
     */
    public void aGagner(String nom) throws IOException;
    
    /**
     * ajoute des cartes de la liste à la liste du jour.
     * 
     * @param cartes - liste de cartes
     */
    public void ajouterCartes(Card[] cartes);
    
    /**
     * @param id 
     * @param cartes
     * @param nbCartes 
     * @throws IOException 
     */
    public void aPiocher(int id, Card[] cartes, int nbCartes) throws IOException;
    
    /**
     * @return couleur
     * @throws IOException 
     */
    public String demanderCouleur() throws IOException;
    
    /**
     * @param id 
     * @param couleur 
     * @throws IOException 
     */
    public void demanderCouleur(int id, String couleur) throws IOException;
    
    /**
     * @return true si nouvelle partie, false sinon
     * @throws IOException 
     */
    public boolean demanderDemarrerNouvellePartie() throws IOException;
    
    /**
     * @return true si terminer partie, false sinon
     * @throws IOException 
     */
    public boolean demanderTerminerPartie() throws IOException;
    
    /**
     * @param message
     * @throws IOException 
     */
    public void envoyerMessage(String message) throws IOException;
    
    /**
     * @return the cartesJouees
     */
    public Card[] getCartesJouees();
    
    /**
     * @return cartes mains
     */
    public ArrayList<Card> getCartesMain();
    
    /**
     * @return the couleur
     */
    public String getCouleur();
    
    /**
     * @return id
     */
    public int getId();

    /**
     * @return the joueurServerRunnable
     */
    public JoueurServerRunnable getJoueurServerRunnable();
    
    /**
     * @return the nbCartesBanque
     */
    public int getNbCartesBanque();
    
    /**
     * @return the nbPoints
     */
    public int getNbPoints();

    /**
     * @return the nom
     */
    public String getNom();
    
    /**
     * @return socket
     */
    public Socket getSocket();

    /**
     * @param i
     * @param carte
     * @return cartes
     * @throws IOException 
     */
    public Card[] jouer(int i, Card carte) throws IOException;

    /**
     * @param cartes the cartes to set
     */
    public void setCartesJouees(Card[] cartes);
    
    /**
     * @param cartes
     * @throws IOException 
     */
    public void setCartesMain(Card[] cartes) throws IOException;

    /**
     * @param couleur the couleur to set
     */
    public void setCouleur(String couleur);

    /**
     * @param joueurServerRunnable the joueurServerRunnable to set
     */
    public void setJoueurServerRunnable(JoueurServerRunnable joueurServerRunnable);

    /**
     * @param nbCartesBanque the nbCartesBanque to set
     * @throws IOException 
     */
    public void setNbCartesBanque(int nbCartesBanque) throws IOException;
    
    /**
     * @param nom the nom to set
     */
    public void setNom(String nom);

    /**
     * @param cartes
     */
    public void supprimerCartes(Card[] cartes);
}
