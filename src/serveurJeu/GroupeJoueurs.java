/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 5 juin 2011
 */

package serveurJeu;

import java.io.IOException;
import java.util.ArrayList;

import commun.Points;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class GroupeJoueurs implements Runnable
{
    private ArrayList<JoueurServer> joueurs;
    private ArrayList<JoueurServer> listeAttente;
    private GestionnairePartie gestionnairePartie;
    private boolean jeuDemarrer;
    private boolean terminer;
    public boolean disconnection = false;
    
    
    /**
     * 
     */
    public GroupeJoueurs()
    {
        this(2);
    }
    
    /**
     * @param nbJoueurs 
     */
    public GroupeJoueurs(int nbJoueurs)
    {
        joueurs = new ArrayList<JoueurServer>(nbJoueurs);
        listeAttente = new ArrayList<JoueurServer>();
        terminer = false;
        jeuDemarrer = false;
        gestionnairePartie = new GestionnairePartie(this);
    }
    
    /**
     * @param joueur
     */
    public void ajouter(JoueurServer joueur)
    {
        try
        {
        	Points pts = new Points();
            if (!gestionnairePartie.finJeu()) {
                terminer = demanderTerminerPartie();
                
                if (terminer) {
                    gestionnairePartie.terminerPartie1();
                    pts = new Points(joueurs.get(0).getPoints().size());
                    if(!pts.isEmpty()) joueur.setPoint(pts.get(pts.size()-1));
                    joueur.setPoints(pts);
                    joueurs.add(joueur);
                    new JoueurServerRunnable(joueur).start();
                    
                } else {
                	pts = new Points(joueurs.get(0).getPoints().size());
            		if(!pts.isEmpty()) joueur.setPoint(pts.get(pts.size()-1));
                	joueur.setPoints(pts);
                    listeAttente.add(joueur);
                }
            } else {
            	if(!joueurs.isEmpty()){
            		pts = new Points(joueurs.get(0).getPoints().size());
                	joueur.setPoints(pts);
            	}
                joueurs.add(joueur);
                new JoueurServerRunnable(joueur).start();
                if(!pts.isEmpty()) joueur.setPoint(pts.get(pts.size()-1));
            }
            
            
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * @param joueur
     */
    public void deconnecter(JoueurServer joueur)
    {
        try {
			disconnection = true;
			joueur.getSocket().close();
			supprimerJoueursDeconnectes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     */
    public boolean supprimerJoueursDeconnectes()
    {
    	int oldSize = joueurs.size();
        for (int i=0; i < joueurs.size(); i++) {
            if (joueurs.get(i).getSocket().isClosed()){
            	joueurs.remove(joueurs.get(i));
            }
        }
        return oldSize != joueurs.size();
    }
    
    /**
     * @param id
     * @return id prochain
     */
    public int prochainId(int id) {
        int j = 0;

        for (int i=0; i < joueurs.size(); i++) {
            if (joueurs.get(i).getId() == id) {
                j = (i + 1 < joueurs.size()) ? (i + 1) : 0;
                break;
            }
        }
        return j;
    }
    
    /**
     * @return joueurPoints
     */
    public ArrayList<JoueurServer> getJoueurs()
    {
        return joueurs;
    }
    
    /**
     * @return ok
     * @throws IOException
     */
    public boolean demanderDemarrerNouvellePartie() throws IOException
    {
    	//supprimerJoueursDeconnectes();
        boolean ok = true;
        for (int i=0; i < joueurs.size() && ok; i++) {
        	if(joueurs.get(i).isConnected() &&
        			!joueurs.get(i).isFerme()) ok = ok && joueurs.get(i).demanderDemarrerNouvellePartie();
        }
        return ok;
    }
    
    /**
     * @return OK
     * @throws IOException
     */
    public boolean demanderTerminerPartie() throws IOException
    {
        boolean ok = true;
        for (int i=0; i < joueurs.size() && ok; i++) {
        	if(joueurs.get(i).isConnected()){
        		if(!joueurs.get(i).isFerme()) ok = ok && joueurs.get(i).demanderTerminerPartie();
        	}
        }
        return ok;
    }
    
    /**
     * @return finJeu
     */
    public boolean finJeu()
    {
        return (!jeuDemarrer);
    }

    /**
     */
    public void demarrerNouvellePartie()
    {
        terminer = false;
    }
    
    /**
     */
    public void terminerPartie()
    {
        terminer = true;
    }
    
    @Override
    public void run()
    {
        while (!terminer) {
            try
            {
                for (int i=0; i < listeAttente.size(); i++) {
                	JoueurServer newJ = listeAttente.remove(i);
                    joueurs.add(newJ);
                    new JoueurServerRunnable(newJ).start();
                }
                jeuDemarrer = demanderDemarrerNouvellePartie();
                if (jeuDemarrer && !disconnection) {
                    
                    for (JoueurServer joueurServer : joueurs) {
                        if(joueurServer.isConnected() && 
                        		!joueurServer.isFerme()) joueurServer.envoyerId();
                        joueurServer.setGestionnairePartie(gestionnairePartie);
                    }
                    Thread thread = new Thread(gestionnairePartie);
                    thread.start();
                    thread.join();
                    disconnection = false;
                    terminer = false;
                } else{
                    terminer = true;
                	gestionnairePartie.terminerPartie1();
                }
            } catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    /**
     * @param id
     * @return nom du joueur
     */
    public String getNom(int id)
    {
    	for(int i=0; i<joueurs.size();i++){
    		if(joueurs.get(i).getId() == id) 
    	        return joueurs.get(i).getNom();
    	}
    	return "Personne!";
    }

    /**
     * @param id
     * @param message
     * @throws IOException
     */
    public void envoyerMessage(int id, String message) throws IOException
    {
        String nom;
        
        nom = "Server";
        if (id != -1) nom = getNom(id);
        
        for (JoueurServer joueurServer : joueurs) {
            if (joueurServer.getId() != id && joueurServer.isConnected()) {
                joueurServer.envoyerMessage(nom, message);
            }
        }
    }
    
    public void envoyerMessage(String message) throws IOException
    {
        for (JoueurServer joueurServer : joueurs) {
            if (joueurServer.isConnected()) {
                joueurServer.envoyerMessage(message);
            }
        }
    }
}
