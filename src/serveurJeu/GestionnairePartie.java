/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 14 juin 2011
 */

package serveurJeu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import commun.Card;
import commun.Cmd;
import java.util.LinkedList;

import javax.swing.Timer;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class GestionnairePartie implements Runnable
{
    private static final int MAX_CARTES = 32;
    
    private GroupeJoueurs groupeJoueurs;
    private ArrayList<JoueurServer> joueurs;

    private JoueurServer joueurCourant;

    private static boolean finJeu = true;
    private Deck banque;
    private ArrayList<Card> tables;
    private ArrayList<Card> fauteCartes;
    
    private int tour, oldId;
    private static int nbCartes;
    private String demande;
    public static boolean prendreDeux, aPiocher;
    public static String etatCourant;
    private Card carteHaute, carteJouees[], OldcarteHaCarte;
    
    private String nomGagnant;
    
    public static boolean faute = false;
    
    
    /**
     * @param groupeJoueurs
     */
    public GestionnairePartie(GroupeJoueurs groupeJoueurs)
    {
        this.groupeJoueurs = groupeJoueurs;
        
        joueurs = groupeJoueurs.getJoueurs();
        banque= new Deck();
        tables = new ArrayList<Card>(MAX_CARTES);
        fauteCartes = new ArrayList<Card>();
        nbCartes = 8;
    }

    public static int getNbCartes() {
        return nbCartes;
    }

    public static void setNbCartes(int nbCartes) {
        GestionnairePartie.nbCartes = nbCartes;
    }
    
    
    /**
     * Batteur de cartesJoueur
     */
    private void battre()
    {
        banque.riffleShuffle(10);
        //battre1();
    }
    
    public void battre1(){
        LinkedList<Card> tmp = new LinkedList<Card>();
        for(int i=0; i<32; i++){
            int k = ((int)(1000*Math.random()))%32;
            while(tmp.contains(banque.get(k))){
                k = ((int)(100*Math.random()))%32;
            }
            tmp.add(banque.get(k));
        }
        
        banque = new Deck(tmp);
    }

    
    private boolean cartesToutes8(Card[] cartes, Card enCour)
    {
        for (int i=0; i < cartes.length; i++) {
            if (!cartes[i].is8())
                return false;
        }
        return true;
    }
    
    private void demanderCouleur() throws IOException
    {
    	//joueurPoints = groupeJoueurs.getJoueurs();
        demande = joueurCourant.demanderCouleur();
        for (int i=0; i < joueurs.size(); i++) {
            joueurs.get(i).demanderCouleur(joueurs.get(tour).getId(), demande);
        }
        passeTour();
        etatCourant = Cmd.JOUER;
    }
    
    /**
     * Partage de cartesJoueur
     */
    private void distribuer(int nbCartes) throws IOException
    {
    	//joueurPoints = groupeJoueurs.getJoueurs();
        nbCartes = Math.min(nbCartes, 32 / joueurs.size());
        Card[] cartes = new Card[nbCartes];
        for (int i=0; i < joueurs.size(); i++) {
            for (int j = 0; j < nbCartes; j++) {
                cartes[j] =  banque.remove(0);
            }
            if(joueurs.get(i).isConnected() && !joueurs.get(i).isFerme()) joueurs.get(i).setCartesMain(cartes);
        }
        for (int i=0; i < joueurs.size(); i++) {
        	if(joueurs.get(i).isConnected() && !joueurs.get(i).isFerme()) joueurs.get(i).setNbCartesBanque(banque.size());
        }
    }
    
    /**
     * @throws IOException
     */
    private void gererPartie() throws IOException
    {
        carteHaute = null;
        oldId = -1;
        tour = 0;
        nomGagnant = "Serveur";
        prendreDeux = false;
        aPiocher = false;
        finJeu = false;
        etatCourant = Cmd.JOUER;
        
        while (!finJeu) {
            if(etatCourant.equals(Cmd.DEMANDER_TERMINER_PARTIE)){
                finJeu = true;
            }
            else if (etatCourant.equals(Cmd.DEMANDER_COULEUR)) {
                demanderCouleur();
                
            } else if (etatCourant.equals(Cmd.A_PIOCHER)) {
            	etatCourant = Cmd.JOUER;
                
            } else if (etatCourant.equals(Cmd.JOUER)) {
            	jouer();
            } else if(etatCourant.equals(Cmd.DECONNECTION)){
                deconnection(joueurCourant);
            }
        }
        
        //Envoie du gagnant et de points
        for (int i=0; i < joueurs.size(); i++) {
        	JoueurServer joueur = joueurs.get(i);
        	if(!joueurs.get(i).isFerme()) joueur.setPoints(joueur.mesPoints(joueur.getCartesMain()));
        }
        for(int i=0;i<joueurs.size();i++){
        	JoueurServer joueur = joueurs.get(i);
        	if(!joueurs.get(i).isFerme()){
	        	joueur.bilan();
	            joueurs.get(i).aGagner(nomGagnant);
            }
        	if(joueur.getPoints().getCompte()>=151) joueur.setFerme(true);
        }
    }
    

    public void deconnection(JoueurServer joueurServer){
    	try {
			groupeJoueurs.envoyerMessage(joueurServer.getNom()+" s'est déconnecté");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		banque.addAll(joueurServer.getCartesMain());
    }
    
    /**
     * @throws IOException
     */
    public void jouer() throws IOException
    {
    	
    	joueurCourant = joueurs.get(tour);
    	if(joueurCourant.isFerme()){
    		passeTour(joueurCourant.getId());
            etatCourant = Cmd.JOUER;
    		return;
    	}
        carteJouees = joueurCourant.jouer(oldId, carteHaute);
        // piocher
        if (carteJouees == null) {
            return;
        }
        
        aPiocher = false;
        etatCourant = Cmd.JOUER;
        
        // verifier si cartes correctes
        if (listeValide(carteJouees, carteHaute)) {
        	
            for(int j=0;j<carteJouees.length;j++){
            	if(!tables.contains(carteJouees[j]))  tables.add(carteJouees[j]);
            }
            
            //
            for (int i=0; i < joueurs.size(); i++) {
                if(joueurs.get(i).isConnected()) joueurs.get(i).aDeposer(joueurs.get(i).getId(), carteJouees);
            }
            
            //
            joueurCourant.supprimerCartes(carteJouees);
            carteHaute = carteJouees[carteJouees.length - 1];

            nomGagnant = groupeJoueurs.getJoueurs().get(tour).getNom(); 
            // tester si carte=Dame, carte=8
            prendreDeux = carteHaute.isAs();
            if (carteHaute.is8()) {
                if (joueurCourant.getCartesMain().isEmpty()) {
                    finJeu = true;
                    groupeJoueurs.disconnection = false;
                } else {
                    etatCourant = Cmd.DEMANDER_COULEUR;
                }
                
            } else if (carteHaute.isDame()) {
                if (joueurCourant.getCartesMain().isEmpty()) {
                    int nbJoueurs = joueurs.size();
                    
                    if (nbJoueurs == 2) {
                        piocher(joueurCourant.getId(), 1);
                        aPiocher = true;
                        
                    } else {
                        finJeu = true;
                    }
                    
                } else {
                    tour = prochainTour();
                    joueurs.get(tour).envoyerMessage("Tour sauté");
                    oldId = tour;
                    tour = prochainTour();
                    joueurs.get(tour).envoyerMessage("A ton tour");
                }
                
            } else {
                passeTour();
            }
            if(joueurCourant.getCartesMain().size() == 2){
                groupeJoueurs.envoyerMessage(-1, joueurCourant.getNom()+" a deux cartes");
            }
            if(joueurCourant.getCartesMain().size() == 1) {
                groupeJoueurs.envoyerMessage(-1, joueurCourant.getNom()+" a une cartes");
            }

            // tester si quelqu'un gagne
            finJeu = joueurCourant.getCartesMain().isEmpty();
        } else {
        	int nbCarte = 2;
        	if(prendreDeux) nbCarte = 3;
        	joueurCourant.faute(nbCarte);
        	passeTour();
        }
    }
    
    public int getJoueurCourantId(){
    	return joueurCourant.getId();
    }
    
    public void faute() throws IOException{
    	
    	carteHaute = OldcarteHaCarte;
    	tables.removeAll(fauteCartes);
    	ArrayList<Card> hands = joueurCourant.getCartesMain();
    	for(Card c:fauteCartes){
    		hands.add(c);
    	}
    	fauteCartes.clear();
    	
    	Card[] cards = new Card[hands.size()];
    	for(int i=0; i<=hands.size();i++){
    		cards[i] = hands.get(i);
    	}
    	joueurCourant.setCartesMain(cards);
    	int nbCarte = 2;
    	if(prendreDeux) nbCarte = 3;
    	joueurCourant.faute(nbCarte);
    	passeTour();
    	
    }
    
    private boolean listeValide(Card[] cartes, Card enCour)
    {
        Card c = cartes[0];
        
        boolean ok = cartesToutes8(cartes, enCour);
        if (ok)
            return true;
        
        if (banque.isEmpty() && tables.isEmpty())
            enCour = null;
        
        if (enCour == null) {
            
            int nbJoueurs = joueurs.size();
            if (nbJoueurs != 2) {
                return (cartes.length == 1);
            }
            
            if (c.isDame()) {
                return valideDame(cartes, enCour);
            }
            return (cartes.length == 1);
        }
        else if (enCour.is8()) {
            
            if (!c.getColor().equals(demande))
                return false;
            
            if (c.isDame()) {
                return valideDame(cartes, enCour);
            }
            return (cartes.length == 1);
        }
        else {
            if (!enCour.isEquivalent(c))
                return false;
            
            if (c.isDame()) {
                return valideDame(cartes, enCour);
            }
            
            if (cartes.length == 1)
                return true;
            
            // meme numero
            for (int i=1; i < cartes.length; i++) {
                if (!cartes[i].sameNumber(enCour))
                    return false;
            }
            return true;
        }
    }
    
    /**
     */
    private void passeTour()
    {
    	oldId = tour;
        tour = prochainTour();
    }
    
    public void passeTour(int id){
    	oldId = tour;
    	tour = prochainId(id);
    }
    
    public int prochainTour(){
    	return (tour+1) % joueurs.size();
    }
    
    public int prochainId(int id) {
        int j = 0;

        for (int i=0; i < joueurs.size(); i++) {
            if (joueurs.get(i).getId() == id) {
                j = (i + 1 < joueurs.size()) ? (i + 1) : 0;
                break;
            }
        }
        return j;
    }
    
    
    public void viderTable()
    {
        while(!tables.isEmpty()){
            banque.add(tables.remove(0));
        }
    }
    
    /**
     * Cette fonction permet aux jouers de prendre une carte à la banque s'ils
     * le desir
     * @param id 
     * @param nbCartes 
     * @throws IOException 
     */
    
    
    public void piocher(int id, int nbCartes) throws IOException
    {
        nbCartes = Math.min(nbCartes,(banque.size()+tables.size()));
        
        Card[] cartes = new Card[nbCartes];

        int k = banque.size();
        if ( k >= nbCartes) {
            for ( int i = 0; i< nbCartes; i++) {
                cartes[i] = banque.remove(0);
            }
        } else {
            
            viderTable();
            
            k = banque.size();
            if ( k >= nbCartes) {
                for ( int i = 0; i< nbCartes; i++) {
                    cartes[i] = banque.remove(0);
                }
            } else {
                int i = 0;
                while(!banque.isEmpty()){
                    cartes[i] = banque.remove(0);
                    i++;
                }
            }
        }
        
        for (int i=0; i < joueurs.size(); i++) {
            if (joueurs.get(i).getId() == id) {
                joueurs.get(i).aPiocher(id, cartes, nbCartes);
                joueurs.get(i).ajouterCartes(cartes);
            }
            else
                joueurs.get(i).aPiocher(id, null, nbCartes);
        }
    }
    
    private boolean valideDame(Card[] cartes, Card enCour)
    {
        Card c = cartes[0];
        
        if (c.isDame()) {
            int i;
            for (i=1; i < cartes.length && cartes[i].isDame(); i++) {
            }
            
            if (i == cartes.length) {
                return true;
            }
            if (cartes[i].is8()) {
                for (; i < cartes.length; i++) {
                    if (!cartes[i].is8())
                        return false;
                }
                return true;
            }
            if (i == cartes.length - 1)
                return (cartes[i - 1].isEquivalent(cartes[i]));
            return false;
        }
        return false;
    }
    
    /**
     * @return the groupeJoueurs
     */
    public GroupeJoueurs getGroupeJoueurs()
    {
        return groupeJoueurs;
    }
    
    /**
     * @param id
     */
    public void passerTour(int id)
    {
        if (joueurs.get(tour).getId() == id && aPiocher || !joueurCourant.isConnected()) {
            aPiocher = false;
            passeTour();
            etatCourant = Cmd.JOUER;
        }
    }
    
    /**
     * @param id
     * @throws IOException
     */
    public void piocher(int id) throws IOException
    {
        if (joueurs.get(tour).getId() == id && !aPiocher) {
            // le joueur veut pioché
            int nbCarte = 1;
            if (prendreDeux) {
                nbCarte = 2;
            }
            piocher(id, nbCarte);
            aPiocher = true;
            
            if (prendreDeux) {
                prendreDeux = false;
                aPiocher = false;
                passeTour();
            }
            etatCourant = Cmd.A_PIOCHER;
        }
    }

    @Override
    public void run()
    {
        try
        {
            // melanger les cartes
            tables.clear();
            banque = new Deck();
            battre();

            // distribuer les cartes
            distribuer(nbCartes);

            gererPartie();
            
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * @param groupeJoueurs the groupeJoueurs to set
     */
    public void setGroupeJoueurs(GroupeJoueurs groupeJoueurs)
    {
        this.groupeJoueurs = groupeJoueurs;
    }

    /**
     * @return finJeu
     */
    public boolean finJeu()
    {
        return finJeu;
    }
    
    /**
     */
    public static void terminerPartie()
    {
        etatCourant = Cmd.DEMANDER_TERMINER_PARTIE;
    }
    
    public void terminerPartie1()
    {
        finJeu = true;
    }
}
