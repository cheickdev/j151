/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 5 juin 2011
 */

package serveurJeu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import commun.Cmd;

import crypto.Crypto;

//import java.util.ArrayList;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class ServeurJeu implements Runnable {
	private final ServerSocket serverSocket;
	private boolean on;
	private GroupeJoueurs groupeJoueurs;

	/**
	 * @param port
	 * @throws IOException
	 */
	public ServeurJeu(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		setOn(true);
	}

	private JoueurServer creerJoueur(int id, Socket socket) throws IOException {
		JoueurServer joueur = new JoueurServer(id, socket);

		BufferedReader istream = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		String ligne = istream.readLine();
		while (ligne != null && !ligne.equals(Cmd.SET_NOM)) {
			ligne = istream.readLine();
		}
		String nom = istream.readLine();
		istream.readLine();// FIN_CMD_N
		nom = Crypto.decoder(nom);
		joueur.setNom(nom);

		return joueur;
	}

	/**
	 * @param on
	 */
	private void setOn(boolean on) {
		this.on = on;
	}

	/**
	 * @throws IOException
	 */
	public void lancer() throws IOException {
		groupeJoueurs = new GroupeJoueurs(2);
		Thread thread = null;
		int id = 0;

		while (on) {
			Socket socket = serverSocket.accept();
			// create new player
			JoueurServer joueur = creerJoueur(id, socket);
			id++;

			// add a player
			groupeJoueurs.ajouter(joueur);

			// Inform others players
			// System.out.println(joueur.getNom());
			groupeJoueurs.envoyerMessage(-1,
					"Nouveau joueur: " + joueur.getNom());

			if (groupeJoueurs.getJoueurs().size() >= 2) {
				if (groupeJoueurs.finJeu()) {

					thread = new Thread(groupeJoueurs);
					groupeJoueurs.demarrerNouvellePartie();
					thread.start();
				} else {

					boolean ok = false;

					ok = groupeJoueurs.demanderTerminerPartie();
					if (ok) {
						groupeJoueurs.terminerPartie();

						thread = new Thread(groupeJoueurs);
						groupeJoueurs.demarrerNouvellePartie();
						thread.start();
					}
				}
			}
		}
	}

	@Override
	public void run() {
		try {
			lancer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		ServeurJeu serveurJeu = new ServeurJeu(7000);
		ArrayList<JoueurServer> joueurPoints = new ArrayList<JoueurServer>();
		int id = 0;
		while (true) {
			Socket socket = serveurJeu.serverSocket.accept();
			JoueurServer joueur = serveurJeu.creerJoueur(id, socket);
			joueurPoints.add(joueur);
			joueur.setNom("Cheick Mahady Sissoko " + id);
			id++;
			// System.out.println(joueur.getNom());
			if (id % 2 == 1)
				joueur.getSocket().close();
			// System.out.println(joueur.isConnected());

			for (JoueurServer joueurServer : joueurPoints) {
				if (joueurServer.isConnected()) {
					System.out.println(joueurServer.getNom() + " est connect�");
				} else {
					joueurServer.envoyerMessage(id, "Bonjour!");
					System.out.println(joueurServer.getNom()
							+ " est deconnect�");
				}
			}
		}
	}
}
