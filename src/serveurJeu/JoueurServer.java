/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 13 juin 2011
 */

package serveurJeu;


import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;


import commun.Card;
import commun.Points;

import java.util.Arrays;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class JoueurServer implements InterfaceJoueur
{
    private static final int MAX_CARTES = 32;
    
    private JoueurServerRunnable joueurServerRunnable;
    private final Socket socket;
    private int id;
    
    private GestionnairePartie gestionnairePartie;
    
    private ArrayList<Card> mains;
    private ArrayList<Card> tables;
    private Points points;
    private Card[] cartesJouees;
    private String couleur;
    private int nbPoints;
    private int nbCartesBanque;
    private String nom;
    private Boolean demarrerPartie = null, terminerPartie;
    private boolean aPiocher, passerTour;

	public boolean deconnxion = false;
    public boolean ferme = false;

	private int v = 0;
    public static boolean bilan = true;
    
    /**
     * @param id 
     * @param socket 
     */
    public JoueurServer(int id, Socket socket)
    {
        this.id = id;
        this.socket = socket;
        tables = new ArrayList<Card>(MAX_CARTES);
        mains = new ArrayList<Card>(MAX_CARTES);
        couleur = null;
        nbPoints = 0;
        points = new Points();
    }
    
    /**
     * @param id 
     * @param cartes
     * @throws IOException 
     */
    @Override
    synchronized public void aDeposer(int id, Card[] cartes) throws IOException
    {
        tables.addAll(Arrays.asList(cartes));
        
        if (this.id == id) {
            supprimerCartes(cartes);
        }
        
        joueurServerRunnable.envADeposer(id, cartes);
        cartesJouees = null;
        notify();
    }

    /**
     * @param i
     * @throws IOException 
     */
    @Override
    public void aGagner(int i) throws IOException
    {
        joueurServerRunnable.envAGagner(i);
    }
    
    /**
     * @param nom 
     * @throws IOException 
     */
    @Override
    public void aGagner(String nom) throws IOException
    {
        joueurServerRunnable.envAGagner(nom);
    }

    /**
     * ajoute des cartes de la liste à la liste du jour.
     * 
     * @param cartes - liste de cartes
     */
    @Override
    public void ajouterCartes(Card[] cartes)
    {
        for(int i=0;i<cartes.length;i++){
        	if(!mains.contains(cartes[i])) mains.add(cartes[i]);
        }
    }

    /**
     * @param id 
     * @param cartes
     * @param nbCartes 
     * @throws IOException 
     */
    @Override
    synchronized public void aPiocher(int id, Card[] cartes, int nbCartes) throws IOException
    {
        if (nbCartesBanque == 0) {
            nbCartesBanque = tables.size();
            tables.clear();
        }
        
        nbCartesBanque -= nbCartes;
        if (nbCartesBanque < 0)
            nbCartesBanque = 0;
        
        if (cartes != null) {
            ajouterCartes(cartes);
        }
        
        joueurServerRunnable.envAPiocher(id, cartes, nbCartes);
    }

    /**
     * @return couleur
     * @throws IOException 
     */
    @Override
    synchronized public String demanderCouleur() throws IOException
    {
        joueurServerRunnable.envDemanderCouleur();
        
        while (couleur == null) {
            try
            {
                wait();
            } catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String tmp = couleur;
        couleur = null;
        return tmp;
    }

    /**
     * @param id 
     * @param couleur 
     * @throws IOException 
     */
    @Override
    public void demanderCouleur(int id, String couleur) throws IOException
    {
        joueurServerRunnable.envDemanderCouleur(id, couleur);
    }

    @Override
    synchronized public boolean demanderDemarrerNouvellePartie() throws IOException
    {
        joueurServerRunnable.envDemanderDemarrerNouvellePartie();
        
        while (demarrerPartie == null) 
        {
            try
            {
                wait();
            } catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        boolean ok = demarrerPartie.booleanValue();
        
        demarrerPartie = null;
        return ok;
    }
    
    @Override
    synchronized public boolean demanderTerminerPartie() throws IOException
    {
        joueurServerRunnable.envDemanderTerminerPartie();
        
        while (terminerPartie == null) {
            try
            {
                wait();
            } catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        boolean ok = terminerPartie.booleanValue();
        terminerPartie = null;
        return ok;
    }
    
    /**
     * @param message
     * @throws IOException 
     */
    @Override
    public void envoyerMessage(String message) throws IOException
    {
        envoyerMessage("Server", message);
    }
    
    /**
     * @param id 
     * @param message
     * @throws IOException 
     */
    public void envoyerMessage(int id, String message) throws IOException
    {
        if (message == null)
            return;
        
        joueurServerRunnable.envoyerMessage(id, message);
        
    }
    
    /**
     * @param nom
     * @param message
     * @throws IOException
     */
    public void envoyerMessage(String nom, String message) throws IOException
    {
        if (message == null)
            return;
        
        joueurServerRunnable.envoyerMessage(nom, message);
    }
    
    /**
     * @return the cartesJouees
     */
    @Override
    public Card[] getCartesJouees()
    {
        return cartesJouees;
    }
    
    /**
     * @return cartes mains
     */
    @Override
    public ArrayList<Card> getCartesMain()
    {
        return mains;
    }
    
    /**
     * @return the couleur
     */
    @Override
    public String getCouleur()
    {
        return couleur;
    }
    
    /**
     * @return the gestionnairePartie
     */
    public GestionnairePartie getGestionnairePartie()
    {
        return gestionnairePartie;
    }
    
    /**
     * @return id
     */
    @Override
    public int getId()
    {
        return id;
    }
    
    /**
     * @return the joueurServerRunnable
     */
    @Override
    public JoueurServerRunnable getJoueurServerRunnable()
    {
        return joueurServerRunnable;
    }
    
    /**
     * @return the nbCartesBanque
     */
    @Override
    public int getNbCartesBanque()
    {
        return nbCartesBanque;
    }
    
    /**
     * @return the nbPoints
     */
    @Override
    public int getNbPoints()
    {
        return nbPoints;
    }

    /**
     * @return the nom
     */
    @Override
    public String getNom()
    {
        return nom;
    }
    
    /**
     * @return socket
     */
    @Override
    public Socket getSocket()
    {
        return socket;
    }
    
    /**
     * @return
     */
    
    public boolean isFerme() {
		return ferme;
	}
    
    /**
     * @param ferme
     */
	public void setFerme(boolean ferme) {
		this.ferme = ferme && bilan;
	}

	/**
     * @param i
     * @param carte
     * @return cartes.
     * @throws IOException 
     */
    @Override
    synchronized public Card[] jouer(int i, Card carte) throws IOException
    {
        joueurServerRunnable.envJouer(i, carte);
        
        while (cartesJouees == null) {
        	if(deconnxion){
        		return null;
        	}
            if (aPiocher) {
                aPiocher = false;
                return null;
            }
            if (passerTour) {
                passerTour = false;
                return null;
            }
            try
            {
                wait();
            } catch (InterruptedException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        Card[] tmp = cartesJouees;
        cartesJouees = null;
        return tmp;
    }

    /**
     * 
     */
    synchronized public void passerTour()
    {
        passerTour = true;
        notify();
        
        gestionnairePartie.passerTour(id);
    }

    /**
     * @throws IOException 
     */
    synchronized public void piocher() throws IOException
    {
        aPiocher = true;
        notify();
        
        gestionnairePartie.piocher(id);
    }
    
    /**
     * @throws IOException 
     */
    synchronized public void piocher(int nbCarte) throws IOException
    {
        aPiocher = true;
        notify();
        
        gestionnairePartie.piocher(id,nbCarte);
    }
    
    /**
     * @param cartes the cartes to set
     */
    @Override
    synchronized public void setCartesJouees(Card[] cartes)
    {
        this.cartesJouees = cartes;
        notify();
    }

    /**
     * @param cartes
     * @throws IOException 
     */
    @Override
    public void setCartesMain(Card[] cartes) throws IOException
    {
        mains.clear();
        ajouterCartes(cartes);
        
        joueurServerRunnable.envSetCartesMain(cartes);
    }

    /**
     * @param couleur the couleur to set
     */
    @Override
    synchronized public void setCouleur(String couleur)
    {
        this.couleur = couleur;
        notify();
    }

    /**
     * @param demarrerPartie the demarrerPartie to set
     */
    synchronized public void setDemarrerPartie(boolean demarrerPartie)
    {
        this.demarrerPartie = demarrerPartie;
        notify();
    }
    
    /**
     * @param gestionnairePartie the gestionnairePartie to set
     */
    public void setGestionnairePartie(GestionnairePartie gestionnairePartie)
    {
        this.gestionnairePartie = gestionnairePartie;
    }
    
    /**
     * @throws IOException 
     */
    public void envoyerId() throws IOException
    {
        joueurServerRunnable.envSetId(id);
    }
    
    /**
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @param joueurServerRunnable the joueurServerRunnable to set
     */
    @Override
    public void setJoueurServerRunnable(JoueurServerRunnable joueurServerRunnable)
    {
        this.joueurServerRunnable = joueurServerRunnable;
    }

    /**
     * @param nbCartesBanque the nbCartesBanque to set
     * @throws IOException 
     */
    @Override
    public void setNbCartesBanque(int nbCartesBanque) throws IOException
    {
        this.nbCartesBanque = nbCartesBanque;
        
        joueurServerRunnable.envSetNbCartesBanque(nbCartesBanque);
    }

    /**
     * @param nom the nom to set
     */
    @Override
    public void setNom(String nom)
    {
    	//System.out.println(nom);
        this.nom = nom;
    }
    
    /**
     * @param terminerPartie the terminerPartie to set
     */
    synchronized public void setTerminerPartie(boolean terminerPartie)
    {
        this.terminerPartie = terminerPartie;
        notify();
    }

    /**
     * @param cartes
     */
    @Override
    public void supprimerCartes(Card[] cartes)
    {
        for (int i=0; i < cartes.length; i++) {
            for (int j=0; j < mains.size(); j++) {
                if (cartes[i].equals(mains.get(j))) {
                    mains.remove(j);
                    break;
                }
            }
        }
    }

    /**
     * @param points
     * @throws IOException 
     */
    public void setPoints(int point) throws IOException
    {
    	if(point==0){
    		v++;
    		if(v==3){
    			nbPoints-=10;
    			v = 0;
    		}
    	} else v = 0;
    	nbPoints+=point;
    	points.add(nbPoints);
        joueurServerRunnable.envSetPoints(nbPoints);
    }
    
    public void setPoint(int nbPoint){
    	nbPoints = nbPoint;
    }
    
    public void faute(int nbCarte) throws IOException{
    	joueurServerRunnable.evnFaute(nbCarte);
    }
    
    public int mesPoints(ArrayList<Card> cartes){
    	if(cartes.isEmpty()) return 0;
    	Card carte = cartes.remove(0);
    	return carte.getValeur()+mesPoints(cartes);
    }

    synchronized public void deconnection() {
    	try {
			socket.close();
			deconnxion = true;
			notify();
			if(gestionnairePartie != null){
				gestionnairePartie.getGroupeJoueurs().deconnecter(this);
				gestionnairePartie.deconnection(this);
			}
			if(gestionnairePartie != null && getId() == gestionnairePartie.getJoueurCourantId()){
				gestionnairePartie.passerTour(getId());
			}
			joueurServerRunnable.stop();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

	public boolean isConnected() {
		return !socket.isClosed();
	}

	public void bilan() throws IOException{
		String bilan = "";
		ArrayList<JoueurServer> joueurs = new ArrayList<JoueurServer>();
		if(gestionnairePartie != null)
		{	
			joueurs = gestionnairePartie.getGroupeJoueurs().getJoueurs();
			for(int i=0; i<joueurs.size();i++){
				bilan = bilan + joueurs.get(i).getNom()+","+joueurs.get(i).getPoints()+"\n";
			}
			joueurServerRunnable.envBilan(bilan);
		}
	}

	public Points getPoints() {
		return points;
	}
	
	public void setPoints(Points pts){
		points = pts;
	}
}
