/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 13 juin 2011
 */

package serveurJeu;

import graphic.JoueurGraphic;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import commun.Card;
import commun.Cmd;
import crypto.Crypto;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class JoueurServerRunnable implements Runnable
{
    private final Socket socket;
    private BufferedReader istream;
    private DataOutputStream ostream;
    
    private JoueurServer joueur;
    private boolean demarrerPartie, terminerPartie;
    public Thread thread = null;
    
    
    /**
     * @param joueur 
     * @param id
     * @param socket 
     * @throws IOException 
     */
    public JoueurServerRunnable(JoueurServer joueur) throws IOException
    {
        this.joueur = joueur;
        socket = joueur.getSocket();
        istream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ostream = new DataOutputStream(socket.getOutputStream());
        
        joueur.setJoueurServerRunnable(this);
        
        thread = new Thread(this);
    }
    
    private void rcvDemanderCouleur() throws IOException
    {
        String couleur;
        
        // recuperer couleur
        couleur = istream.readLine();
        istream.readLine();// FIN_CMD_N
        
        joueur.setCouleur(couleur);
    }

    private void rcvDemanderDemarrerNouvellePartie() throws IOException
    {
        String ligne = istream.readLine();
        istream.readLine();// FIN_CMD_N
        
        demarrerPartie = (ligne != null && ligne.equals("true"));
        joueur.setDemarrerPartie(demarrerPartie);
    }

    private void rcvDemanderTerminerPartie() throws IOException
    {
        String ligne = istream.readLine();
        istream.readLine();// FIN_CMD_N
        
        terminerPartie = (ligne != null && ligne.equals("true")); 
        joueur.setTerminerPartie(terminerPartie);
    }

    private void rcvGetNom() throws IOException
    {
        int id = Integer.parseInt(istream.readLine());
        istream.readLine();// FIN_CMD_N
        
        String nom = "Server";
        if (id != -1)
            nom = joueur.getGestionnairePartie().getGroupeJoueurs().getNom(id);
        
        ostream.writeBytes(Cmd.GET_NOM + "\n");
        ostream.writeBytes(Crypto.coder(nom) + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    private void rcvSetNom() throws IOException
    {
        String nom = istream.readLine();
        istream.readLine();// FIN_CMD_N
        nom = Crypto.decoder(nom);
        joueur.setNom(nom);
    }
    
    private void rcvJouer() throws IOException
    {
        String ligne = istream.readLine();
        ligne = istream.readLine();//id
        int taille = Integer.parseInt(ligne);
        Card[] cartes = new Card[taille];
        
        int j = 0;
        ligne = istream.readLine();
        while (ligne != null && j < taille) {
            cartes[j] = Card.creerCarte(ligne);
            j++;
            ligne = istream.readLine();
        }
        joueur.setCartesJouees(cartes);
    }
    
    private void rcvPasserTour() throws IOException
    {
        istream.readLine();// FIN_CMD_N
        
        // Piocher
        joueur.passerTour();
    }
    
    private void rcvFaute(int nbCarte) throws IOException
    {
        joueur.piocher(nbCarte);
    }

    private void rcvPiocher() throws IOException
    {
    	String ligne = istream.readLine();// FIN_CMD_N or nbCarte
    	if(ligne.equals(Cmd.FIN_CMD)) joueur.piocher();
    	else{
    		int nbCarte = Integer.parseInt(ligne);
    		rcvPiocher(nbCarte);
    	}
    }
    
    private void rcvPiocher(int nbCarte) throws IOException
    {
        joueur.piocher(nbCarte);
		istream.readLine();//FIN_CMD_N
    }

    private void recevoirMessage() throws IOException
    {
        String ligne = istream.readLine();
        int id = Integer.parseInt(ligne);
        ligne = istream.readLine();
        int taille = Integer.parseInt(ligne);
        
        StringBuilder message = new StringBuilder(taille);
        int c = istream.read();
        while (c != -1 && taille > 0) {
            message.append((char) c);
            taille--;
            c = istream.read();
        }
        ligne = istream.readLine();//FIN_CMD_N
        String msg = Crypto.decoder(message.toString());
        joueur.getGestionnairePartie().getGroupeJoueurs().envoyerMessage(id, msg);
    }
    
    /**
     * @param id
     * @param cartes
     * @throws IOException
     */
    public void envADeposer(int id, Card[] cartes) throws IOException
    {
        ostream.writeBytes(Cmd.A_DEPOSER + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(cartes.length + "\n");
        for (int j=0; j < cartes.length; j++) {
            ostream.writeBytes(cartes[j].toString() + "\n");
        }
        ostream.writeBytes(Cmd.FIN_CMD_N);
        
    }

    /**
     * @param i
     * @throws IOException
     */
    public void envAGagner(int i) throws IOException
    {
        ostream.writeBytes(Cmd.A_GAGNER + "\n");
        ostream.writeBytes(i + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @param nom 
     * @throws IOException
     */
    public void envAGagner(String nom) throws IOException
    {
        ostream.writeBytes(Cmd.A_GAGNER + "\n");
        ostream.writeBytes(nom + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param id
     * @param cartes
     * @param nbCartes
     * @throws IOException
     */
    public void envAPiocher(int id, Card[] cartes, int nbCartes) throws IOException
    {
        ostream.writeBytes(Cmd.A_PIOCHER + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(nbCartes + "\n");
        if (cartes != null) {
            for (int j=0; j < cartes.length; j++) {
                ostream.writeBytes(cartes[j].toString() + "\n");
            }
        }
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @throws IOException
     */
    public void envDemanderCouleur() throws IOException
    {
        ostream.writeBytes(Cmd.DEMANDER_COULEUR + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param id
     * @param couleur
     * @throws IOException
     */
    public void envDemanderCouleur(int id, String couleur) throws IOException
    {
        ostream.writeBytes(Cmd.A_DEMANDER_COULEUR + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(couleur + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @throws IOException 
     */
    public void envDemanderDemarrerNouvellePartie() throws IOException
    {
        ostream.writeBytes(Cmd.DEMANDER_DEMARRER_NOUVELLE_PARTIE + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    
    /**
     * @throws IOException 
     */
    public void envDemanderTerminerPartie() throws IOException
    {
        ostream.writeBytes(Cmd.DEMANDER_TERMINER_PARTIE + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param i
     * @param carte
     * @throws IOException 
     */
    public void envJouer(int i, Card carte) throws IOException
    {
        ostream.writeBytes(Cmd.JOUER + "\n");
        ostream.writeBytes(i + "\n");
        ostream.writeBytes(carte + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param id 
     * @param message
     * @throws IOException
     */
    public void envoyerMessage(int id, String message) throws IOException
    {
    	message = Crypto.coder(message);
        ostream.writeBytes(Cmd.MESSAGE + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(message.length() + "\n");
        ostream.writeBytes(message);
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param nom
     * @param message
     * @throws IOException
     */
    public void envoyerMessage(String nom, String message) throws IOException
    {
    	//System.out.println(message);
    	String msg = Crypto.coder(message);
        ostream.writeBytes(Cmd.MESSAGE + "\n");
        ostream.writeBytes(Crypto.coder(nom) + "\n");
        ostream.writeBytes(msg.length() + "\n");
        ostream.writeBytes(msg);
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @param cartes
     * @throws IOException
     */
    public void envSetCartesMain(Card[] cartes) throws IOException
    {
        ostream.writeBytes(Cmd.SET_CARTES + "\n");
        ostream.writeBytes(cartes.length + "\n");
        for (int i=0; i < cartes.length; i++) {
            ostream.writeBytes(cartes[i].toString());
            ostream.writeBytes("\n");
        }
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * Envoyer id
     * @param id 
     * @throws IOException 
     */
    public void envSetId(int id) throws IOException
    {
        ostream.writeBytes(Cmd.SET_ID + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @param nbCartesBanque
     * @throws IOException
     */
    public void envSetNbCartesBanque(int nbCartesBanque) throws IOException
    {
        ostream.writeBytes(Cmd.SET_NB_CARTES_BANQUE + "\n");
        ostream.writeBytes(nbCartesBanque + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @param points
     * @throws IOException
     */
    public void envSetPoints(int point) throws IOException
    {
        ostream.writeBytes(Cmd.POINTS + "\n");
        ostream.writeBytes(point + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    /**
     * @return the joueur
     */
    public JoueurServer getJoueur()
    {
        return joueur;
    }

    public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public Socket getSocket() {
		return socket;
	}

	@Override
    public void run()
    {
        String ligne;
        
        try
        {
            ligne = istream.readLine();
            while (ligne != null) {
//                System.out.println(ligne + "$$$");
                if (ligne.equals(Cmd.DEMANDER_COULEUR)) {
                    rcvDemanderCouleur();
                    
                } else if (ligne.equals(Cmd.DEMANDER_DEMARRER_NOUVELLE_PARTIE)) {
                    rcvDemanderDemarrerNouvellePartie();
                    
                } else if (ligne.equals(Cmd.DEMANDER_TERMINER_PARTIE)) {
                    rcvDemanderTerminerPartie();
                    
                } else if (ligne.equals(Cmd.JOUER)) {
                    rcvJouer();
                    
                } else if (ligne.equals(Cmd.PASSER_TOUR)) {
                    rcvPasserTour();
                    
                } else if (ligne.equals(Cmd.PIOCHER)) {
                	rcvPiocher();
                    
                } else if (ligne.equals(Cmd.MESSAGE)) {
                    
                    recevoirMessage();
                    
                } else if (ligne.equals(Cmd.GET_NOM)) {
                    rcvGetNom();
                    
                } else if (ligne.equals(Cmd.SET_NOM)) {
                    rcvSetNom();
                    
                } else if(ligne.equals(Cmd.DECONNECTION)){
                    rcvDeconnection();
                } else if(ligne.equals(Cmd.FAUTE)){
                	ligne = istream.readLine();
                	int nbCarte = 2;
            		nbCarte = Integer.parseInt(ligne);
            		rcvFaute(nbCarte);
            		istream.readLine();//FIN_CMD_N
                } else if(ligne.equals(Cmd.BILAN)){
                	rcvBilan();
                }
                ligne = istream.readLine();
            }
            
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void rcvBilan() throws IOException {
		istream.readLine();// FIN_CMD_N
		joueur.bilan();
	}

	/**
     * @param joueur the joueur to set
     */
    public void setJoueur(JoueurServer joueur)
    {
        this.joueur = joueur;
    }

    private void rcvDeconnection() throws IOException {
        istream.readLine();// FIN_CMD_N
        joueur.deconnection();
    }

	public void evnFaute(int nbCarte) throws IOException {
		ostream.writeBytes(Cmd.FAUTE+"\n");
		ostream.writeBytes(""+nbCarte+"\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
	}

	public void start() {
		thread.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop(){
		thread.stop();
	}

	public void envBilan(String bilan) throws IOException {
		ostream.writeBytes(Cmd.BILAN + "\n");
		ostream.writeBytes(bilan);
		ostream.writeBytes(Cmd.FIN_CMD_N);
	}
}
