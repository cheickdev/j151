/**
 */

package commun;

/**
 * 
 */
public class Card {
    /*Les attributs de la classe Carte*/
    private String numero, couleur;
    private int poids = 0;
    
    public static final int X = 125,Y = 150;
    
    /**Création d'une carte par son numéro et sa couleur
     * @param numero
     * @param couleur  
     */
    public Card(String numero, String couleur) 
    {
        this.numero = numero;
        this.couleur = couleur;
        if(numero.equals("10")) poids = 0;
        else if(numero.equals("9")) poids = 1;
        else if(numero.equals("7")) poids = 2;
        else if(numero.equals("R")) poids = 3;
        else if(numero.equals("V")) poids = 4;
        else if(numero.equals("D")) poids = 5;
        else if(numero.equals("1")) poids = 6;
        else if(numero.equals("8")) poids = 7;
    }
	
    /**Retourne la couleur de la carte
     * @return couleur
     */
    public String getColor() {
        return couleur;
    }
	
    /** retourne le numéro de la carte
     * @return numero
     */
    public String getNumber() {
        return numero;
    }
	
    /**Renvoi la valeur de la carte
     * @return la valeur de la carte
     */
    public int getValeur(){
        if(numero.equals("1")) return 11;
        else if(numero.equals("V")) return 2;
        else if(numero.equals("D")) return 3;
        else if(numero.equals("R")) return 4;
        else if(numero.equals("8")) return 32;        
        else return Integer.parseInt(numero);
    }
	
    public int getPoids() {
        return poids;
    }
    
    /**Change la couleur de la carte
     * @param couleur 
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
	
    /**Change le numéro de la carte
     * @param numero 
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    /**L'equivalence de deux cartes
     * @param c
     * @return  
     */
    public boolean isEquivalent(Card c){
        if(c != null && c.is8()) return true;
        if (this != null && isAs()) return c != null && c.isAs();
        return (this == null ? c == null : memeCouleur(c) || sameNumber(c));
    }
	
    /**vérifie si deux cartes ont meme couleur
     * @param c
     * @return  
     */
    public boolean sameNumber(Card c){
        return numero.equals(c.getNumber());
    }
    
    /**
     * @param c
     * @return  
     */
    public boolean memeCouleur(Card c){
        return couleur.equals(c.getColor());
    }
    /**Verifie si la carte un neutralisant
     * @return 
     */
    public boolean is8(){
        return numero.equals("8");
    }
    
    /**Verifie si la carte un as
     * @return 
     */
    public boolean isAs(){
        return numero.equals("1");
    }
    
    /**Verifie si la carte un as
     * @return 
     */
    public boolean isDame(){
        return numero.equals("D");
    }
    
    /**Format textuel d'une carte
     * @return 
     */
    @Override
    public String toString(){
        if (this == null) return "null";
        return "["+numero+","+couleur+"]";
    }
    
    /**
     * @param ligne
     * @return carte
     */
    public static Card creerCarte(String ligne)
    {
        int i = ligne.indexOf(',');
        String numero = ligne.substring(1, i);
        String couleur = ligne.substring(i+1, ligne.length() - 1);
        Card c = new Card(numero, couleur);
        return c;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Card) {
            Card c = (Card) obj;
            return ((numero == null ? c.getNumber() == null : numero.equals(c.getNumber())) && 
                    (couleur == null ? c.getColor() == null : couleur.equals(c.getColor())));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.numero != null ? this.numero.hashCode() : 0);
        hash = 43 * hash + (this.couleur != null ? this.couleur.hashCode() : 0);
        return hash;
    }
    
    public static String listeNumero[] = {"1","V","D","R","7","8","9","10"};
    public static String listeCouleur[] = {"pique","coeur","trefle","carreau"};
}