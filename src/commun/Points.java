/**
 * 
 */
package commun;

import java.util.ArrayList;

/**
 * @author Mcicheick
 *
 */
public class Points extends ArrayList<Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3058502099038546431L;

	/**
	 * 
	 */
	public Points() {
		super();
	}
	
	public Points(int l){
		super();
		for(int i=0; i<l;i++){
			add(10*(i+1));
		}
	}
	
	public boolean add(Integer integer){
		 return super.add(integer);
	}
	
	public int getCompte(){
		if(isEmpty()) return 0;
		return get(size()-1);
	}
	
	public String[] toArray(){
		String[] res = new String[size()];
		for(int i=0;i<size();i++){
			res[i] = get(i)+"";
		}
		return res;
	}

	@Override
	public String toString(){
		int arg0 = 0;
		if(!isEmpty()) arg0 = get(0);
		String s = arg0+"";
		for(int i=1;i<size();i++){
			s = s+","+get(i);
		}
		return s;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Points points = new Points();
		points.add(10);
		points.add(14);
		points.add(70);
		System.out.println(points);
	}

}
