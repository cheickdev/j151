/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Mcicheick
 */
public class BanqueClickAction extends MouseAdapter{
    AbstractJoueur joueur;

    public BanqueClickAction(AbstractJoueur joueur) {
        this.joueur = joueur;
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (!joueur.aMain())
            return;
        
        joueur.piocher();
    }
}
