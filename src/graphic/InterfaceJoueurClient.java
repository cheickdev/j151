/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 7 juin 2011
 */

package graphic;

import java.io.IOException;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public interface InterfaceJoueurClient
{
    /**
     * @throws IOException 
     */
    public void aDemanderCouleur() throws IOException;

    /**
     * @throws IOException
     */
    public void aDeposer() throws IOException;

    /**
     * @throws IOException
     */
    public void aGagner() throws IOException;
    
    /**
     * @throws IOException
     */
    public void aGagner2() throws IOException;

    /**
     * @throws IOException
     */
    public void aPiocher() throws IOException;

    /**
     * Envoye au serveur la couleur que le joueur demande.
     * 
     * @throws IOException
     */
    public void demanderCouleur() throws IOException;

    /**
     * @throws IOException
     */
    public void demanderDemarrerNouvellePartie() throws IOException;

    /**
     * @throws IOException 
     */
    public void demanderTerminerPartie() throws IOException;

    /**
     * Envoye un message au joueur.
     * 
     * @param message
     * @throws IOException
     */
    public void envoyerMessage(String message) throws IOException;

    /**
     * @return indice du joueur
     */
    public int getId();

    /**
     * @throws IOException 
     */
    public void setId() throws IOException;

    /**
     * @param nom 
     * @throws IOException 
     */
    public void setNom(String nom) throws IOException;
    
    /**
     * @param cartes - cartes deposées par le joueur précedent
     * @throws IOException
     */
    public void jouer() throws IOException;


    /**
     * @throws IOException
     */
    public void piocher() throws IOException;

    /**
     * @throws IOException
     */
    public void points() throws IOException;

    /**
     * @throws IOException
     */
    public void setCartesBanque() throws IOException;

    /**
     * @throws IOException
     */
    public void setCartesMain() throws IOException;
    
    /**
     * @throws IOException 
     */
    public void passerTour() throws IOException;
    
    /**
     * Retrouver le nom du joueur
     * @param id
     * @return nom du joueur
     * @throws IOException
     */
    public String getJoueurNom(int id) throws IOException;

	public void faute() throws IOException;

	public void piocher(int nbCarte) throws IOException;
}
