/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JPanel;

import commun.Card;
import commun.Cmd;

/**
 * 
 * @author Mcicheick
 */
public class BanquePane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image image = null;
	AbstractJoueur joueur;
	int nbBanque = 0;
	Toolkit kit;

	public BanquePane(AbstractJoueur joueur) {
		this.joueur = joueur;
		kit = Toolkit.getDefaultToolkit();
		nbBanque = joueur.getNbCartesBanque();
		this.addMouseListener(new BanqueClickAction(joueur));
	}

	@Override
	public void paintComponent(Graphics g) {
		// g.drawImage(kit.getImage("Cartes/table.png"), 0, 0,this.getWidth(),
		// this.getHeight(),this);
		image = kit.getImage(getClass().getResource(Cmd.cartePath + "dos.png"));
		for (int i = 0; i < nbBanque; i++) {
			g.drawImage(image, (this.getHeight() - 2 * nbBanque - Card.X / 2)
					/ 2 + i * 2, (this.getHeight() - Card.Y) / 2, Card.X,
					Card.Y, this);
		}
	}

	public void rafraichir() {

		nbBanque = joueur.getNbCartesBanque();

		repaint();
	}
}
