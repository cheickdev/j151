/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;

import javax.swing.Timer;

import commun.Card;
import commun.Cmd;

/**
 *
 * @author Mcicheick
 */
public class CartePaneGraphic extends AbstractCartePane {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CartePaneGraphic(Card carte, AbstractJoueur abstractJoueur) {
		super(carte, abstractJoueur);

		if (carte.isAs())
			toolTipText = "As de " + carte.getColor();
		else if (carte.isDame())
			toolTipText = "Dame de " + carte.getColor();
		else if (carte.getNumber().equals("R"))
			toolTipText = "Roi de " + carte.getColor();
		else if (carte.getNumber().equals("V"))
			toolTipText = "Vall\u00E9e de " + carte.getColor();
		else
			toolTipText = carte.getNumber() + " de " + carte.getColor();

		kit = Toolkit.getDefaultToolkit();

		image = kit.getImage(getClass().getResource(
				Cmd.cartePath + carte.getNumber() + carte.getColor() + ".png"));

		addMouseListener(new ActionClick() {
			@Override
			public void mouseClicked(MouseEvent me) {
				clickActionListener(me);
			}
		});

		setSize(Card.X, Card.Y);

		dest = new Point(10, (180 - Card.Y) / 2);

		setBorder(javax.swing.BorderFactory.createTitledBorder(""));

		setToolTipText(toolTipText);

	}

	@Override
	public void deplacement(Point a, Point b) {
		srcX = a.x;
		srcY = a.y;
		destX = b.x;
		destY = b.y;
		nbPasX = (destX - srcX) / 10;
		nbPasY = (destY - srcY) / 10;

		Timer timer = new Timer(50, new TransferTask(this, srcX, srcY, destX,
				destY, nbPasX, nbPasY) {

			@Override
			public void actionPerformed(ActionEvent ev) {
				timerEventListener(ev);
			}
		});
		timer.start();
	}

	public void clickActionListener(MouseEvent me) {

		if (!isClickable())
			return;

		if (!abstractJoueur.aMain())
			return;

		CartePaneGraphic cards;

		cards = (CartePaneGraphic) me.getSource();

		if (!haut && me.isControlDown()) {
			setLocation(getX(), getY() - 20);
			haut = true;
		} else if (haut && me.isControlDown()) {
			setLocation(getX(), getY() + 20);
			CarteJoueurPane.array.remove(cards.carte);
			if (CarteJoueurPane.array.isEmpty())
				CarteJoueurPane.uneCarte = true;
			haut = false;
			return;
		}

		if (me.isControlDown()) {

			CarteJoueurPane.uneCarte = false;
			Card card = cards.carte;
			if (!CarteJoueurPane.array.contains(card))
				CarteJoueurPane.array.add(card);

		} else if (CarteJoueurPane.uneCarte) {

			Card card = cards.carte;
			CarteJoueurPane.liste = new Card[1];
			CarteJoueurPane.liste[0] = card;
			abstractJoueur.setCartes(CarteJoueurPane.liste);
			CarteJoueurPane.uneCarte = true;

		} else {

			int k = CarteJoueurPane.array.size();
			CarteJoueurPane.liste = new Card[k];

			for (int i = 0; i < k; i++) {
				CarteJoueurPane.liste[i] = CarteJoueurPane.array.get(i);
			}
			if (k != 0) {
				abstractJoueur.setCartes(CarteJoueurPane.liste);
			}
			CarteJoueurPane.uneCarte = true;
			CarteJoueurPane.array.clear();

		}
	}

	public void timerEventListener(ActionEvent ev) {

		Timer timer;

		timer = (Timer) ev.getSource();
		if (nbPasX > 0) {
			if (x < destX) {
				cartePane.setLocation(x, y);
				x += nbPasX;
			} else {
				cartePane.setLocation(destX, destY);
				timer.stop();
			}
		} else if (nbPasX < 0) {
			if (x > destX) {
				cartePane.setLocation(x, y);
				x += nbPasX;
			} else {
				cartePane.setLocation(destX, destY);
				timer.stop();
			}
		}
		if (nbPasY > 0) {
			if (y < destY) {
				cartePane.setLocation(x, y);
				y += nbPasY;
			} else {
				cartePane.setLocation(destX, destY);
				timer.stop();
			}
		} else if (nbPasY < 0) {
			if (y > destY) {
				cartePane.setLocation(x, y);
				y += nbPasY;
			} else {
				cartePane.setLocation(destX, destY);
				timer.stop();
			}
		}
	}

}
