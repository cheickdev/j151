package graphic;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class TestJTextArea extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5462230357659708765L;

	/**
	 * @param args
	 */
	private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
	public TestJTextArea() {
		initComponents();
	}
	
	
    private void initComponents() {

		setSize(200, 200);
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jTextArea1.setEditable(true);
        jTextArea1.setSize(150,100);
        jTextArea1.setLocation(20, 20);
        
        jScrollPane1.setViewportView(jTextArea1);
        jScrollPane1.setAutoscrolls(true);
        jScrollPane1.setLocation(10, 10);
        jScrollPane1.setSize(180, 110);
        
        add(jScrollPane1);
        setLayout(null);
	}
	
	public static void main(String[] args) {
		new TestJTextArea().setVisible(true);
	}
	
	

}
