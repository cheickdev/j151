/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JLayeredPane;

import commun.Cmd;

/**
 * @author Mcicheick
 */
public class PanePrincipal extends JLayeredPane {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image image = null;
	Toolkit kit;

	public PanePrincipal() {
		kit = Toolkit.getDefaultToolkit();
		image = kit.getImage(getClass()
				.getResource(Cmd.cartePath + "table.png"));
	}

	public PanePrincipal(String path) {
		kit = Toolkit.getDefaultToolkit();
		image = kit.getImage(getClass().getResource(path));
	}

	public PanePrincipal(Image image) {
		kit = Toolkit.getDefaultToolkit();
		this.image = image;
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
	}

	public void rafraichir() {
		image = kit.getImage(getClass()
				.getResource(Cmd.cartePath + "table.png"));
	}

	public void rafraichir(Image image) {
		this.image = image;
		repaint();
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image2) {
		image = image2;
	}
}
