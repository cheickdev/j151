/**
 * author: TRAORÉ Bilal && SISSOKO Cheick Mahady version: 0.0.1 date: 5 juin
 * 2011
 */
package graphic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

import commun.Card;
import commun.Cmd;
import commun.Points;
import java.io.BufferedInputStream;

/**
 * @author TRAORÉ Bilal && Cheick Mahady SISSOKO
 * @version 0.0.1
 */
public abstract class AbstractJoueur {

    public static final int MAX_CARTES = 32;
    protected JoueurClient joueurClient;
    protected ArrayList<Card> handCards;
    protected ArrayList<Card> tables;
    protected Points points;
    protected boolean enBilan;
    protected String couleur;
    protected int nbCartesBanque;
    protected int nbPoints;
    protected String nom;
    protected InputStream stream;
    protected MaFenetre maFenetre;
    protected Card[] cartes;
    protected boolean aMain = false;
    protected String message = null;
    protected Bilan bilan;
    protected boolean aPiocher = false, passTour = false, faute = false;

    /**
     * @param joueurClient
     */
    public AbstractJoueur(JoueurClient joueurClient) {
        this.joueurClient = joueurClient;
        this.joueurClient.setJoueur(this);
        tables = new ArrayList<Card>(MAX_CARTES);
        handCards = new ArrayList<Card>(MAX_CARTES);
        points = new Points();
        couleur = null;
        nbPoints = 0;
        bilan = new Bilan();
    }

    public AbstractJoueur(MaFenetre maFenetre) {

        tables = new ArrayList<Card>(MAX_CARTES);
        handCards = new ArrayList<Card>(MAX_CARTES);
        couleur = null;
        nbPoints = 0;
    }

    /**
     * @return maFenetre
     */
    public MaFenetre getMaFenetre() {

        return maFenetre;
    }

    public void setMaFenetre(MaFenetre maFenetre) {
        this.maFenetre = maFenetre;
    }

    public Card[] getCartes() {
        return cartes;
    }

    /**
     *
     * @param cartes
     */
    public void setCartes(Card[] cartes) {
        this.cartes = cartes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        // System.out.println(message);
        this.message = message;
    }

    /**
     *
     * @param i
     * @param cartes
     */
    public void aDeposer(int id, Card[] cartes) {

        tables.addAll(Arrays.asList(cartes));

        if (id == joueurClient.getId()) {
            supprimerCartes(cartes);

            aMain = false;
        }
    }

    /**
     * @param i
     */
    public void aGagner(int i) {
    }

    /**
     * @param nom
     */
    public void aGagner(String nom) {
    }

    /**
     * ajoute des cartes de la liste à la liste du jour.
     *
     * @param cartes - liste de cartes
     */
    public void ajouterCartes(Card[] cartes) {
        handCards.addAll(Arrays.asList(cartes));
    }

    /**
     * @param i
     * @param cartes
     * @param nbCartes
     */
    public void aPiocher(int i, Card[] cartes, int nbCartes) {

        if (nbCartesBanque == 0) {
            nbCartesBanque = tables.size();
            tables.clear();
        }

        nbCartesBanque -= nbCartes;
        if (nbCartesBanque <= 0) {
            nbCartesBanque = 0;
        }

        if (cartes != null) {

            ajouterCartes(cartes);

        }

    }

    /**
     * Le joueur joue
     *
     * @throws IOException
     */
    public void jouer() throws IOException {
        joueurClient.jouer();
    }

    /**
     * @return couleur demandée par le joueur.
     */
    abstract public String demanderCouleur();

    /**
     * @return true si nouvelle partie, false sinon
     */
    abstract public boolean demanderDemarrerNouvellePartie();

    /**
     * @return cartes mains
     */
    public ArrayList<Card> getCartesMain() {
        return handCards;
    }

    /**
     * @return cartes tables
     */
    public ArrayList<Card> getTables() {
        return tables;
    }

    /**
     * @return the couleur
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * @return the joueurClient
     */
    public JoueurClient getJoueurClient() {
        return joueurClient;
    }

    /**
     * @return the nbCartesBanque
     */
    public int getNbCartesBanque() {
        return nbCartesBanque;
    }

    /**
     * @return the nbPoints
     */
    public int getNbPoints() {
        return nbPoints;
    }

    /**
     * @param i
     * @param carte
     * @return cartes jouées.
     */
    abstract public Card[] jouer(int i, Card carte);

    /**
     *
     */
    public void passerTour() {
        try {
            joueurClient.passerTour();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void faute() {
        try {
            joueurClient.faute();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     */
    synchronized public void piocher() {
        try {
            joueurClient.piocher();
            notify();

        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    /**
     * @param cartes
     */
    public void setCartesMain(Card[] cartes) {
        // System.out.println("cartes Main");
        handCards.clear();
        tables.clear();
        ajouterCartes(cartes);

    }

    /**
     * @param couleur the couleur to set
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /**
     * @param joueurClient the joueurClient to set
     */
    public void setJoueurClient(JoueurClient joueurClient) {
        this.joueurClient = joueurClient;
    }

    /**
     * @param nbCartesBanque the nbCartesBanque to set
     */
    public void setNbCartesBanque(int nbCartesBanque) {
        this.nbCartesBanque = nbCartesBanque;
    }

    /**
     * @param nbPoints the nbPoints to set
     */
    public void setNbPoints(int nbPoints) {
        points.add(nbPoints);
        this.nbPoints = nbPoints;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean aMain() {
        return aMain;
    }

    /**
     * @param message
     */
    public void envoyerMessage(String message) {
        try {
            joueurClient.envoyerMessage(message);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    abstract public boolean demanderTerminerPartie();

    /**
     * @param cartes
     */
    public void supprimerCartes(Card[] cartes) {
        for (int i = 0; i < cartes.length; i++) {
            for (int j = 0; j < handCards.size(); j++) {
                if (cartes[i].equals(handCards.get(j))) {
                    handCards.remove(j);
                    break;
                }
            }
        }
    }

    /**
     * Lecture du fichier audio passé en paramètre
     *
     * @param file
     */
    public void playChemin(File file) {
        try {
            AudioInputStream pstream = AudioSystem.getAudioInputStream(file);

            AudioFormat format = pstream.getFormat();

            // conversion
            if (format.getEncoding() == AudioFormat.Encoding.ALAW
                    || format.getEncoding() == AudioFormat.Encoding.ULAW) {
                AudioFormat tmp = new AudioFormat(
                        AudioFormat.Encoding.PCM_SIGNED,
                        format.getSampleRate(),
                        format.getSampleSizeInBits() * 2, format.getChannels(),
                        format.getFrameSize() * 2, format.getFrameRate(), true);
                pstream = AudioSystem.getAudioInputStream(tmp, pstream);
                format = tmp;
            }
            long milliseconds = (long) ((pstream.getFrameLength() * 1000) / pstream
                    .getFormat().getFrameRate());
            double duration = milliseconds / 1000.0;
            SourceDataLine sourceLine = AudioSystem.getSourceDataLine(format);

            sourceLine.open();
            sourceLine.start();

            int size = 128 * 1024;// 128kb
            byte[] data = new byte[size];
            int nbBytes = 0;
            while ((nbBytes = pstream.read(data, 0, data.length)) != -1) {
                if (nbBytes >= 0) {
                    sourceLine.write(data, 0, nbBytes);
                }
            }

            sourceLine.drain();
            sourceLine.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * La lecture du fichier audio
     *
     * @param demande
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void play(String demande) throws FileNotFoundException, IOException {
        if (MaFenetre.activer) {
            InputStream ips = getClass().getResourceAsStream(
                    Cmd.audioPath + demande + Cmd.AE);
            if (ips != null) {
                playChemin(ips);
            } else {
                System.err.println(Cmd.audioPath + demande + Cmd.AE);
            }
        }
    }

    /**
     * @param ips
     */
    private void playChemin(InputStream ips) {
        try {

            //add buffer for mark/reset support
            InputStream bufferedIn = new BufferedInputStream(ips);
            AudioInputStream pstream = AudioSystem.getAudioInputStream(bufferedIn);

            AudioFormat format = pstream.getFormat();

            // conversion
            if (format.getEncoding() == AudioFormat.Encoding.ALAW
                    || format.getEncoding() == AudioFormat.Encoding.ULAW) {
                AudioFormat tmp = new AudioFormat(
                        AudioFormat.Encoding.PCM_SIGNED,
                        format.getSampleRate(),
                        format.getSampleSizeInBits() * 2, format.getChannels(),
                        format.getFrameSize() * 2, format.getFrameRate(), true);
                pstream = AudioSystem.getAudioInputStream(tmp, pstream);
                format = tmp;
            }
            long milliseconds = (long) ((pstream.getFrameLength() * 1000) / pstream
                    .getFormat().getFrameRate());
            double duration = milliseconds / 1000.0;
            SourceDataLine sourceLine = AudioSystem.getSourceDataLine(format);

            sourceLine.open();
            sourceLine.start();

            int size = 128 * 1024;// 128kb
            byte[] data = new byte[size];
            int nbBytes = 0;
            while ((nbBytes = pstream.read(data, 0, data.length)) != -1) {
                if (nbBytes >= 0) {
                    sourceLine.write(data, 0, nbBytes);
                }
            }

            sourceLine.drain();
            sourceLine.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void setBilan(ArrayList<String[]> list) {
        bilan = new Bilan(list);
    }

    public Bilan getBilan() {
        return bilan;
    }
}
