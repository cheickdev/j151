/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import commun.Card;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Mcicheick
 */
public class CarteOrdiPane extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JoueurOrdinateur joueur;
    ArrayList<Image> images = new ArrayList<Image>();
    Toolkit kit;
    public CarteOrdiPane(JoueurOrdinateur joueur) {
        this.joueur = joueur;
        kit=Toolkit.getDefaultToolkit();
        setImages(joueur.getCartesMain());
    }
    
    
    @Override
    public void paintComponent(Graphics g){
        int k = images.size();
        g.drawImage(kit.getImage("Cartes/table.png"), 0, 0,this.getWidth(), this.getHeight(),this);
        for(int i=0; i<k; i++){
            g.drawImage(images.get(i),5+i*5,20,125, 175, this);
        }
    }
    
    public void rafraichir(ArrayList<Card> cartes){
        setImages(cartes);
        repaint();
    }
    
    public void setImages(ArrayList<Card> cartes){
        int k = cartes.size();
        for (int i = 0; i < k; i++){
            images.add(kit.getImage("Carte/dos.png"));
        }
    }
}
