/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.Timer;

import commun.Card;
import commun.Cmd;

/**
 * 25062011131225
 * 
 * @author Mcicheick
 */
public class CartePane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image image = null;
	Card carte;
	Toolkit kit;
	AbstractJoueur abstractJoueur;
	boolean haut = false;
	Point dest;
	String toolTipText;

	private boolean clickable = false;

	boolean uneCarte = true;

	public CartePane(Card carte, AbstractJoueur abstractJoueur) {

		this.abstractJoueur = abstractJoueur;

		this.carte = carte;

		if (carte.isAs())
			toolTipText = "As de " + carte.getColor();
		else if (carte.isDame())
			toolTipText = "Dame de " + carte.getColor();
		else if (carte.getNumber().equals("R"))
			toolTipText = "Roi de " + carte.getColor();
		else if (carte.getNumber().equals("V"))
			toolTipText = "Vall\u00E9e de " + carte.getColor();
		else
			toolTipText = carte.getNumber() + " de " + carte.getColor();

		kit = Toolkit.getDefaultToolkit();

		image = kit.getImage(getClass().getResource(
				"../" + Cmd.cartePath + carte.getNumber() + carte.getColor()
						+ Cmd.CE));

		addMouseListener(new ActionClick());

		setSize(Card.X, Card.Y);

		dest = new Point(10, (180 - Card.Y) / 2);

		setBorder(javax.swing.BorderFactory.createTitledBorder(""));

		setToolTipText(toolTipText);
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
	}

	public void deplacement(Point a, Point b) {
		int srcX, srcY, destX, destY, nbPasX, nbPasY;
		srcX = a.x;
		srcY = a.y;
		destX = b.x;
		destY = b.y;
		nbPasX = (destX - srcX) / 10;
		nbPasY = (destY - srcY) / 10;

		Timer timer = new Timer(50, new TransferTask(this, srcX, srcY, destX,
				destY, nbPasX, nbPasY));
		timer.start();
	}

	public boolean isClickable() {
		return clickable;
	}

	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	public Point getDest() {
		return dest;
	}

	public void setDest(Point dest) {
		this.dest = dest;
	}

	class ActionClick extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent me) {

			if (!isClickable())
				return;

			if (!abstractJoueur.aMain())
				return;

			CartePane cartePane;

			cartePane = (CartePane) me.getSource();

			if (!haut && me.isControlDown()) {
				setLocation(getX(), getY() - 20);
				haut = true;
			} else if (haut && me.isControlDown()) {
				setLocation(getX(), getY() + 20);
				CarteJoueurPane.array.remove(cartePane.carte);
				if (CarteJoueurPane.array.isEmpty())
					CarteJoueurPane.uneCarte = true;
				haut = false;
				return;
			}

			if (me.isControlDown()) {

				CarteJoueurPane.uneCarte = false;
				Card carte = cartePane.carte;
				if (!CarteJoueurPane.array.contains(carte))
					CarteJoueurPane.array.add(carte);

			} else if (CarteJoueurPane.uneCarte) {

				Card carte = cartePane.carte;
				CarteJoueurPane.liste = new Card[1];
				CarteJoueurPane.liste[0] = carte;
				abstractJoueur.setCartes(CarteJoueurPane.liste);
				CarteJoueurPane.uneCarte = true;

			} else {

				int k = CarteJoueurPane.array.size();
				CarteJoueurPane.liste = new Card[k];

				for (int i = 0; i < k; i++) {
					CarteJoueurPane.liste[i] = CarteJoueurPane.array.get(i);
				}
				if (k != 0) {
					abstractJoueur.setCartes(CarteJoueurPane.liste);
				}
				CarteJoueurPane.uneCarte = true;
				CarteJoueurPane.array.clear();

			}
		}

	}

	class ActionClick2 extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent me) {
			dest = new Point(
					10 + 30 * ((abstractJoueur.getTables().size()) % (510 / 30)),
					148 - Card.Y);
			deplacement(new Point(getX(), getY()), dest);
		}

	}

	class TransferTask implements ActionListener {
		CartePane cartePane;
		int srcX, srcY, destX, destY, nbPasX, nbPasY;
		int x, y;

		TransferTask(CartePane cartePane, int srcX, int srcY, int destX,
				int destY, int nbPasX, int nbPasY) {
			this.cartePane = cartePane;
			this.srcX = srcX;
			this.srcY = srcY;
			this.destX = destX;
			this.destY = destY;
			this.nbPasX = nbPasX;
			this.nbPasY = nbPasY;

			x = srcX;
			y = srcY;
		}

		@Override
		public void actionPerformed(ActionEvent ev) {
			Timer timer;

			timer = (Timer) ev.getSource();
			if (nbPasX > 0) {
				if (x < destX) {
					cartePane.setLocation(x, y);
					x += nbPasX;
				} else {
					cartePane.setLocation(destX, destY);
					timer.stop();
				}
			} else if (nbPasX < 0) {
				if (x > destX) {
					cartePane.setLocation(x, y);
					x += nbPasX;
				} else {
					cartePane.setLocation(destX, destY);
					timer.stop();
				}
			}
			if (nbPasY > 0) {
				if (y < destY) {
					cartePane.setLocation(x, y);
					y += nbPasY;
				} else {
					cartePane.setLocation(destX, destY);
					timer.stop();
				}
			} else if (nbPasY < 0) {
				if (y > destY) {
					cartePane.setLocation(x, y);
					y += nbPasY;
				} else {
					cartePane.setLocation(destX, destY);
					timer.stop();
				}
			}
		}
	}
}
