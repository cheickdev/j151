/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;

/**
 *
 * @author Mcicheick
 */
public class LancementPane extends JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Image image = null;
    Toolkit kit;
    public LancementPane() {
        kit=Toolkit.getDefaultToolkit();
        image = kit.getImage("Cartes/table.png");
        if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
            setMacMenuBar();
        }
    }
    @Override
    public void paintComponent(Graphics g){
        g.drawImage(image, 0, 0,this.getWidth(), this.getHeight(),this);
    }
    
    private void setMacMenuBar()
    {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
    }
    
    
}
