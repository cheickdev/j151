/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import commun.Card;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 *
 * @author Mcicheick
 */
abstract  class AbstractCartePane extends JPanel{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -7661106637702009137L;
	Image image = null;
    Card carte;
    Toolkit kit;
    AbstractJoueur abstractJoueur;
    boolean haut = false;
    Point dest;
    String toolTipText;
    
    private boolean clickable = false;
    boolean uneCarte = true;
    
    CartePane cartePane;
    int srcX,srcY, destX, destY, nbPasX, nbPasY;
    int x, y;

    public AbstractCartePane(Card carte, AbstractJoueur abstractJoueur) {
        
        this.abstractJoueur = abstractJoueur;
        this.carte = carte;
        setSize(Card.X, Card.Y);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }
    
    abstract public void deplacement(Point a, Point b);

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }
    
    public Point getDest() {
        return dest;
    }

    public void setDest(Point dest) {
        this.dest = dest;
    }
    
    abstract public class ActionClick extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent me){}
        
    };
    
    abstract public class ActionClick2 extends MouseAdapter{
        
        @Override
        public void mouseClicked(MouseEvent me) {
            dest = new Point(10 + 30*((abstractJoueur.getTables().size())%(510/30)),148-Card.Y);
            deplacement(new Point(getX(),getY()), dest);
        }
    
    };
    
    abstract public class TransferTask implements ActionListener{

        public TransferTask(AbstractCartePane cartePane, int srcX, int srcY, int destX, 
                int destY, int nbPasX, int nbPasY) {
        }
        
    };
}
