
/**
 * author: TRAORÉ Bilal && 
 * SISSOKO Cheick Mahady
 * version: 0.0.1
 * date: 5 juin 2011
 */

package graphic;

import commun.Card;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */

public class JoueurGraphic extends AbstractJoueur implements Runnable
{
    /**
     * @param joueurClient
     */
    
    public static String demande;
    public static boolean demandeNouvellePartie, 
            terminer = false,demandeTerminerPartie;
    private Thread thread = null;
	private boolean deconnxion;
	public boolean newM = false;
    
    public JoueurGraphic(JoueurClient joueurClient)
    {
        super(joueurClient);
        //joueurClient.setJoueurGraphic(this);
        thread = new Thread(this);
    }
    
    

    @Override
    public MaFenetre getMaFenetre() {
        return super.getMaFenetre();
    }

    @Override
    public void setMaFenetre(MaFenetre maFenetre) {
        super.setMaFenetre(maFenetre);
    }
    

    @Override
    public void aDeposer(int i, Card[] cartes)
    {
        super.aDeposer(i, cartes);
        
        JoueurOrdinateur.hands = handCards;
        maFenetre.demandeLabel.setText("");
        
        rangerList();
        
        //maFenetre.carteTablePane.rafraichir();
        //maFenetre.carteJoueurPane.rafraichir(getCartesMain());
        maFenetre.rafraichir();
        
    }

    @Override
    synchronized public void piocher() 
    {
        super.piocher();
        aPiocher = true;
        notify();
        
    }
    
    public void passer(){
        piocher();
    }
    
    @Override
    public void aGagner(int i)
    {
        super.aGagner(i);
        
        JOptionPane.showMessageDialog(null, "Le joueur "+i+" a gagné");
        
        terminer = true;
        tables.clear();
        //maFenetre.carteTablePane.rafraichir();
        maFenetre.rafraichir();
    }
    
    @Override
    public void aGagner(String nom)
    {
        super.aGagner(nom);
        
        JOptionPane.showMessageDialog(null, nom+" a gagné");
        
        terminer = true;
        tables.clear();
        //maFenetre.carteTablePane.rafraichir();
        maFenetre.rafraichir();
    }
    
    @Override
    public void aPiocher(int i, Card[] cartes, int nbCartes)
    {
        super.aPiocher(i, cartes, nbCartes);
        
        
        maFenetre.banquePane.rafraichir();
//        maFenetre.carteTablePane.rafraichir();
//        maFenetre.carteJoueurPane.rafraichir(getCartesMain());
//        maFenetre.carteJoueurPane.repaint();
        maFenetre.rafraichir();
    }
    
    @Override
    public String demanderCouleur()
    {
        new DemandeCouleur(maFenetre, true).setVisible(true);
        couleur = demande;
        return demande;
    }
    
    @Override
    public boolean demanderDemarrerNouvellePartie()
    {
        new DemandeNouvellePartie(maFenetre, true).setVisible(true);
        return demandeNouvellePartie;
    }
    
    
    @Override
    synchronized public Card[] jouer(int i, Card carte)
    {
        aMain = true;
		passTour = false;
		
        maFenetre.tourLabel.setText("A ton tour!");
        maFenetre.requestFocus();
        
        while(cartes == null) {
            
            try {
            	if(newM && aMain){
            		try {
						joueurClient.recevoirMessage();
						newM = false;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	
            	if(deconnxion){
                    maFenetre.tourLabel.setText("");
            		return null;
            	}
            	
            	if(faute){
            		faute = false;
            		aPiocher = true;
            		passTour = true;
                    cartes = null;
            	}
                if (aPiocher) {
                    aPiocher = false;
                    aMain = false;
                    maFenetre.tourLabel.setText("");
                    return null;
                }
                if (passTour){
                    passTour = false;
                    aMain = false;
                    maFenetre.tourLabel.setText("");
                    return null;
                }
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(JoueurGraphic.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        maFenetre.tourLabel.setText("");
        
        Card[] tmp = cartes;
        cartes = null;
        return tmp;
    }

    @Override
    public void setCouleur(String couleur)
    {
        super.setCouleur(couleur);
        try {
            maFenetre.demandeLabel.setText(couleur);
            play(couleur);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JoueurGraphic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JoueurGraphic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void setNbCartesBanque(int nbCartesBanque)
    {
        super.setNbCartesBanque(nbCartesBanque);
        
        maFenetre.banquePane.rafraichir();
    }
    
    @Override
    public void setCartesMain(Card[] cartes)
    {
        super.setCartesMain(cartes);
        JoueurOrdinateur.hands = handCards;
        
        rangerList();
        
        maFenetre.messageText.setText("");
        //maFenetre.carteJoueurPane.rafraichir(getCartesMain());
        maFenetre.rafraichir();
    }

    @Override
    public void setMessage(String message) {
        
        super.setMessage(message);
        
        String oldM = maFenetre.messageText.getText();

        if (!oldM.isEmpty()) maFenetre.messageText.append("\n"+message);
        else maFenetre.messageText.setText(message);
    }

    @Override
    synchronized public void envoyerMessage(String message) {
        super.envoyerMessage(message);
        notify();
    }
    
    

    @Override
    public String getMessage() {
        return super.getMessage();
    }
    
    public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}



	@Override
    synchronized public void setCartes(Card[] cartes) {
        super.setCartes(cartes);
        notify();
    }

    @Override
    public boolean demanderTerminerPartie() {
    	DemandeNouvellePartie np = new DemandeNouvellePartie(null, true);
    	np.setTitle("Terminer partie");
    	np.jLabel1.setText("Voulez-vous terminer la partie ?");
    	np.setVisible(true);
        return demandeTerminerPartie;
    }

    @Override
    synchronized public void passerTour() {
        super.passerTour();
        passTour = true;
        notify();
    }
    
    @Override
    synchronized public void faute() {
        super.faute();
        notify();
    }

    @Override
    public void setNbPoints(int nbPoints) {
        points.add(this.nbPoints+nbPoints);
        super.setNbPoints(nbPoints);
    }
    
    public void rangerList() {
        int k = handCards.size();
        ArrayList<Card> liste = new ArrayList<Card>();
        for (int p = 0; p < 8; p++) {
            k = handCards.size();
            for (int i = 0; i < k; i++) {
                if (handCards.get(i).getPoids() == p) {
                    liste.add(handCards.get(i));
                }
            }
        }
        handCards = liste;
    }

	public void start() {
		thread.start();
	}
	
	synchronized public void stop() {
		deconnxion = true;
		terminer = true;
		notify();
	}
	
	public void nouveauMessage(){
		notify();
	}

    @SuppressWarnings("deprecation")
	@Override
    public void run() {
        
        while(!terminer){
        	if(newM && aMain) nouveauMessage();
        }
		thread.stop();
    }
    
}
