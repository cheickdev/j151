/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphic;

import commun.Card;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;

/**
 *
 * @author Mcicheick
 */
public class CartePaneBanque extends AbstractCartePane{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CartePaneBanque(Card carte, AbstractJoueur abstractJoueur) {
        super(carte, abstractJoueur);
        
        kit=Toolkit.getDefaultToolkit();
        
        image = kit.getImage("Cartes/dos.png");
        
        addMouseListener(new ActionClick() {
            @Override
            public void mouseClicked(MouseEvent me){
                clickActionListener(me);
            }
        });
        
        setSize(Card.X, Card.Y);
        
        
        dest = new Point(10, (180-Card.Y)/2);
        
        setBorder(javax.swing.BorderFactory.createTitledBorder(""));
    }

    @Override
    public void deplacement(Point a, Point b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public void clickActionListener(MouseEvent me){
        
        if (!abstractJoueur.aMain())
            return;
        
        abstractJoueur.piocher();
    }
    
    
    
}
