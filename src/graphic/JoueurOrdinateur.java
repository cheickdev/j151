/**
 * author: TRAORÉ Bilal && 
 * SISSOKO Cheick Mahady
 * version: 0.0.1
 * date: 5 juin 2011
 */

package graphic;

import commun.Card;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import serveurJeu.Deck;

public class JoueurOrdinateur extends AbstractJoueur {
	/**
	 * @param joueurClient
	 */
	boolean apiocher = false;

	boolean graphic = false;

	static int level = 1;
	public static int mill = 500;

	ArrayList<Card> laTable = new ArrayList<Card>();
	ArrayList<Card> laBanque = new ArrayList<Card>();
	ArrayList<Card> saMain = new ArrayList<Card>();

	public static ArrayList<Card> hands = new ArrayList<Card>();
	public static ArrayList<Card> deck = new ArrayList<Card>();

	public JoueurOrdinateur(JoueurClient joueurClient) {
		super(joueurClient);
		setNom("Ordinateur");
	}

	@Override
	public void aGagner(int i) {
		super.aGagner(i);
	}

	@Override
	public void aGagner(String nom) {
		init();
		if (graphic)
			JOptionPane.showMessageDialog(maFenetre, nom + " a gagné");
	}

	private void init() {
		saMain.clear();
		laBanque.clear();
		laTable.clear();
	}

	@Override
	public void aPiocher(int i, Card[] cartes, int nbCartes) {
		super.aPiocher(i, cartes, nbCartes);
		for (int j = 0; j < nbCartes; j++) {
			Card tmp;
			if (!laBanque.isEmpty()) {
				tmp = laBanque.remove(0);
				if (i != joueurClient.getId()) {
					saMain.add(tmp);
				}
			} else if (!laTable.isEmpty()) {
				tmp = laTable.remove(0);
				if (i != joueurClient.getId()) {
					saMain.add(tmp);
				}
			}
		}
		if (laBanque.isEmpty())
			viderTable();

		if (graphic) {
			maFenetre.banquePane.rafraichir();
			maFenetre.rafraichir();
		}
	}

	public Card[] pioche() {

		if (!apiocher) {
			piocher();
			apiocher = true;
			if (graphic)
				maFenetre.tourLabel.setText("");
			return null;
		} else {
			passerTour();
			apiocher = false;
			if (graphic)
				maFenetre.tourLabel.setText("");
			try {
				play("passe");
			} catch (FileNotFoundException ex) {
				Logger.getLogger(JoueurOrdinateur.class.getName()).log(
						Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(JoueurOrdinateur.class.getName()).log(
						Level.SEVERE, null, ex);
			}
			return null;
		}
	}

	@Override
	public Card[] jouer(int i, Card carte) {

		try {
			Thread.sleep(mill);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (graphic) {
			maFenetre.tourLabel.setText("A ton tour!");
			maFenetre.requestFocus();
		}

		if (level == 1)
			return jouer1(i, carte);
		if (level == 2)
			return jouer2(i, carte);
		if (level == 3)
			return jouer3(i, carte);
		if (level == 4)
			return jouer4(i, carte);
		return jouer1(i, carte);
	}

	public Card[] play(int i, Card carte) {
		if (level == 1)
			return jouer1(i, carte);
		if (level == 2)
			return jouer2(i, carte);
		if (level == 3)
			return jouer3(i, carte);
		if (level == 4)
			return jouer4(i, carte);
		return jouer1(i, carte);
	}

	public Card[] jouer1(int i, Card carte) {
		ArrayList<Card> tmp = new ArrayList<Card>();
		ArrayList<Card> copie = new ArrayList<Card>();
		rangerList();

		int nbCartes;
		Card[] cartes1 = null;
		copie = handCards;
		if (carte == null) {
			Card maCarte = handCards.get(0);
			tmp.add(maCarte);
			nbCartes = 1;
			cartes1 = new Card[nbCartes];
			cartes1[0] = tmp.get(0);
		} else if (carte.is8()) {

			String color = getCouleur();

			Card maCarte = handCards.get(0);
			int k = handCards.size();

			for (int j = 0; j < k
					&& !(maCarte.getColor().equals(color) || maCarte.is8()); j++) {
				maCarte = handCards.get(j);
			}
			if (maCarte.getColor().equals(color) || maCarte.is8()) {
				tmp.add(maCarte);
				nbCartes = 1;
				cartes1 = new Card[nbCartes];
				cartes1[0] = tmp.get(0);
			} else {
				return pioche();
			}
		} else {
			int k = copie.size();
			Card maCarte = null;

			if (k != 0)
				maCarte = copie.get(0);

			for (int j = 0; j < k && !carte.isEquivalent(maCarte); j++) {
				maCarte = copie.get(j);
			}
			if (carte.isEquivalent(maCarte)) {
				int sec;
				sec = nbSecJoueur(carte);
				if (maCarte.sameNumber(carte)) {
					if (sec >= 1) {
						for (int p = 0; p < sec; p++) {
							int kk = copie.size();
							for (int j = 0; j < kk; j++) {
								maCarte = copie.get(j);
								if (maCarte.sameNumber(carte)) {
									copie.remove(maCarte);
									tmp.add(maCarte);
									break;
								}
							}
						}
					}
				} else
					tmp.add(maCarte);

				nbCartes = tmp.size();
				cartes1 = new Card[nbCartes];
				for (int j = 0; j < nbCartes; j++) {
					cartes1[j] = tmp.get(j);
				}
			} else {
				return pioche();
			}
		}
		if (graphic)
			maFenetre.tourLabel.setText("");
		return cartes1;
	}

	public Card[] jouer2(int i, Card card) {

		Card[] cards;
		ArrayList<Card> tmp = new ArrayList<Card>();
		ArrayList<Card> eqCards = new ArrayList<Card>();

		rangerList2();

		ArrayList<Card> reste = reste();
		String coul[] = { "pique", "coeur", "trefle", "carreau" };
		for (int k = 0; k < 4; k++) {
			String tm = "";
			for (int j = k + 1; j < 4; j++) {
				if (nbColor(coul[k], reste) >= nbColor(coul[j], reste)) {
					tm = coul[k];
					coul[k] = coul[j];
					coul[j] = tm;
				}
			}
		}

		if (card == null) {
			for (Card c : handCards) {
				if (!c.is8() && occurance(c, reste()).size() <= 1)
					return new Card[] { c };
			}
			for (Card c : handCards) {
				if (!c.is8() && occurance(c, reste()).size() <= 2)
					return new Card[] { c };
			}
			if (graphic)
				maFenetre.tourLabel.setText("");
			return new Card[] { handCards.get(0) };
		}
		for (Card c : handCards) {
			if (card.isEquivalent(c))
				eqCards.add(c);
		}

		if (saMain.size() <= 2 && containsNumber("8", handCards)) {

			for (Card c : handCards) {
				if (graphic)
					maFenetre.tourLabel.setText("");
				if (c.is8())
					return new Card[] { c };
			}
		}

		if (saMain.size() == 1 && !laBanque.isEmpty()
				&& laBanque.get(0) != null && laBanque.get(0).is8()) {
			if (graphic)
				maFenetre.tourLabel.setText("");
			return pioche();
		}

		if (card.is8()) {
			String color = getCouleur();
			for (Card c : handCards) {
				if (color.equals(c.getColor())
						&& occurance(c, reste()).size() <= 1) {
					if (graphic)
						maFenetre.tourLabel.setText("");
					return new Card[] { c };
				}
			}
			for (Card c : handCards) {
				if (color.equals(c.getColor())
						&& occurance(c, reste()).size() <= 2) {
					if (graphic)
						maFenetre.tourLabel.setText("");
					return new Card[] { c };
				}
			}
			for (Card c : handCards) {
				if (color.equals(c.getColor()) || c.is8()) {
					if (graphic)
						maFenetre.tourLabel.setText("");
					return new Card[] { c };
				}
			}
			return pioche();
		}

		if (!eqCards.isEmpty()) {
			for (Card c : eqCards) {
				if (c.is8() && occurance(c, handCards).size() == handCards.size()) {
					tmp = occurance(c, handCards);
					cards = new Card[tmp.size()];
					for (int j = 0; j < cards.length; j++) {
						cards[j] = tmp.get(j);
					}
					if (graphic)
						maFenetre.tourLabel.setText("");
					return cards;
				}
				if (occurance(c, reste()).size() <= 1) {
					if (card.isAs() && card.sameNumber(c)
							&& occurance(c, reste()).isEmpty()) {
						tmp = occurance(c, eqCards);
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						if (graphic)
							maFenetre.tourLabel.setText("");
						return cards;
					}
					if (card.isAs() && card.sameNumber(c)) {
						if (graphic)
							maFenetre.tourLabel.setText("");
						return new Card[] { c };
					}
					if (saMain.size() == 1) {
						tmp = occurance(c, eqCards);
						if (containsColor(tmp.get(tmp.size() - 1).getColor(),
								reste()) && tmp.size() == 1) {
							return pioche();
						}
						if (containsColor(tmp.get(tmp.size() - 1).getColor(),
								reste())) {
							tmp.remove(tmp.size() - 1);
						}
						if (c.is8()) {
							if (graphic)
								maFenetre.tourLabel.setText("");
							return new Card[] { c };
						}
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						if (graphic)
							maFenetre.tourLabel.setText("");
						return cards;
					}
					if (card.sameNumber(c)) {
						tmp = occurance(c, eqCards);
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						if (graphic)
							maFenetre.tourLabel.setText("");
						return cards;
					}
					if (c.is8()) {
						if (graphic)
							maFenetre.tourLabel.setText("");
						return new Card[] { c };
					}
					if (c.isAs()) {
						tmp = occurance(c, eqCards);
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						if (graphic)
							maFenetre.tourLabel.setText("");
						return cards;
					}
					if (graphic)
						maFenetre.tourLabel.setText("");
					return new Card[] { c };
				}
			}

			for (int j = 0; j < 4; j++) {
				for (Card c : eqCards) {
					if (coul[j].equals(c.getColor()) && !c.is8()
							&& card.isEquivalent(c)) {
						if (graphic)
							maFenetre.tourLabel.setText("");
						return new Card[] { c };
					}
				}
			}
			if (graphic)
				maFenetre.tourLabel.setText("");
			return new Card[] { eqCards.get(0) };

		} else {
			return pioche();
		}
	}

	public Card[] jouer3(int i, Card carte) {
		Card[] cards;
		ArrayList<Card> tmp = new ArrayList<Card>();
		ArrayList<Card> ceq = new ArrayList<Card>();

		rangerList2();

		for (Card c : handCards) {
			if (carte.isEquivalent(c))
				ceq.add(c);
		}
		if (carte == null) {
			for (Card c : handCards) {
				if (!c.is8() && occurance(c, hands).size() <= 1)
					return new Card[] { c };
			}
			for (Card c : handCards) {
				if (!c.is8() && occurance(c, hands).size() <= 2)
					return new Card[] { c };
			}
			return new Card[] { handCards.get(0) };
		}

		if (carte.is8()) {
			String color = getCouleur();
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (tmp.size() == handCards.size()) {
					cards = new Card[tmp.size()];
					for (int j = 0; j < cards.length; j++) {
						cards[j] = tmp.get(j);
					}
					return cards;
				}
			}
			for (Card c : handCards) {
				if (!c.is8() && occurance(c, hands).size() <= 1
						&& occurance(c, hands).size() != hands.size()
						&& color.equals(c.getColor()))
					return new Card[] { c };
			}
			for (Card c : handCards) {
				if (c.is8()) {
					tmp = occurance(c, handCards);
					if (tmp.size() == handCards.size()) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
					return new Card[] { c };
				}
			}
			return pioche();
		}
		if (!ceq.isEmpty()) {
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (tmp.size() == handCards.size()) {
					cards = new Card[tmp.size()];
					for (int j = 0; j < cards.length; j++) {
						cards[j] = tmp.get(j);
					}
					return cards;
				}
			}
			if (hands.size() <= 2) {
				for (Card c : ceq) {
					int nbn = nbNumber(c.getNumber(), hands);
					int nbc = nbColor(c.getColor(), hands);
					if (nbn == 0 && nbc == 0 && !deck.isEmpty()
							&& !c.isEquivalent(deck.get(0))) {
						return new Card[] { c };
					}
				}
				for (Card c : ceq) {
					if (c.isAs() && nbNumber("1", hands) == 0)
						return new Card[] { c };
				}
				for (Card c : ceq) {
					if (c.is8()) {
						return new Card[] { c };
					}
				}
			}
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2 && c.isAs()
						&& nbNumber("1", hands) == 0) {
					cards = new Card[tmp.size()];
					for (int j = 0; j < cards.length; j++) {
						cards[j] = tmp.get(j);
					}
					return cards;
				}
			}
			for (Card c : ceq) {
				if (c.sameNumber(carte) && tmp.size() >= 2 && c.isAs()) {
					return new Card[] { c };
				}
			}
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2) {
					Card last = tmp.get(tmp.size() - 1);
					Card first = tmp.get(tmp.size() - 1);
					if (nbNumber(last.getColor(), hands) == 0
							&& !deck.isEmpty()
							&& !last.isEquivalent(deck.get(0))
							&& (nbColor(last.getColor(), handCards) != 0)) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}

					if (nbNumber(first.getColor(), hands) == 0
							&& !deck.isEmpty()
							&& !first.isEquivalent(deck.get(0))
							&& (nbColor(first.getColor(), handCards) != 0)) {
						tmp.add(tmp.remove(0));
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
				}
			}
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2) {
					Card last = tmp.get(tmp.size() - 1);
					Card first = tmp.get(tmp.size() - 1);

					if (nbNumber(last.getColor(), hands) == 0
							&& !deck.isEmpty()
							&& !last.isEquivalent(deck.get(0))) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}

					if (nbNumber(first.getColor(), hands) == 0
							&& !deck.isEmpty()
							&& !first.isEquivalent(deck.get(0))) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
				}
			}
			for (Card c : ceq) {
				tmp = occurance(c, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2 && !c.is8()) {
					Card last = tmp.get(tmp.size() - 1);
					Card first = tmp.get(tmp.size() - 1);

					if (nbNumber(last.getColor(), hands) == 0) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}

					if (nbNumber(first.getColor(), hands) == 0) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
				}
			}

			for (Card c : ceq) {
				tmp = occurance(carte, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2) {
					Card last = tmp.get(tmp.size() - 1);
					Card first = tmp.get(tmp.size() - 1);
					if (!deck.isEmpty() && !last.isEquivalent(deck.get(0))
							&& (nbColor(last.getColor(), handCards) != 0)) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}

					if (!deck.isEmpty() && !first.isEquivalent(deck.get(0))
							&& (nbColor(first.getColor(), handCards) != 0)) {
						tmp.add(tmp.remove(0));
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
				}
			}
			for (Card c : ceq) {
				tmp = occurance(carte, ceq);
				if (c.sameNumber(carte) && tmp.size() >= 2) {
					Card last = tmp.get(tmp.size() - 1);
					Card first = tmp.get(tmp.size() - 1);

					if (!deck.isEmpty() && !last.isEquivalent(deck.get(0))) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}

					if (!deck.isEmpty() && !first.isEquivalent(deck.get(0))) {
						cards = new Card[tmp.size()];
						for (int j = 0; j < cards.length; j++) {
							cards[j] = tmp.get(j);
						}
						return cards;
					}
					cards = new Card[tmp.size()];
					for (int j = 0; j < cards.length; j++) {
						cards[j] = tmp.get(j);
					}
					return cards;
				}
			}

			for (Card c : ceq) {
				int nbn = nbNumber(c.getNumber(), hands);
				int nbc = nbColor(c.getColor(), hands);

				if (nbn == 0 && nbc == 0 && !deck.isEmpty()
						&& !c.isEquivalent(deck.get(0))) {
					return new Card[] { c };
				}
			}
			for (Card c : ceq) {
				int nbn = nbNumber(c.getNumber(), hands);
				int nbc = nbColor(c.getColor(), hands);

				if (nbc == 0 && nbn == 0 && !deck.isEmpty()
						&& !deck.get(0).is8() && !deck.get(0).isAs()) {
					return new Card[] { c };
				}
			}
			for (Card c : ceq) {
				int nbn = nbNumber(c.getNumber(), hands);
				int nbc = nbColor(c.getColor(), hands);

				if (nbc == 0 && nbn == 0 && !deck.isEmpty()
						&& !deck.get(0).is8()) {
					if (deck.get(0).isAs() && nbNumber("1", handCards) >= 1) {
						return new Card[] { c };
					}
				}
			}
			for (Card c : ceq) {
				int nbn = nbNumber(c.getNumber(), hands);
				int nbc = nbColor(c.getColor(), hands);

				if (nbc == 0 && nbn == 0) {
					return new Card[] { c };
				}
			}
			for (Card c : ceq) {
				int nbn = nbNumber(c.getNumber(), hands);

				if (nbn <= 1 && !c.is8()) {
					return new Card[] { c };
				}
			}
			for (Card c : ceq) {
				int nbc = nbColor(c.getNumber(), hands);
				if (nbc == 0) {
					return new Card[] { c };
				}
			}
			System.out.println("Fin");
			return new Card[] { ceq.get(0) };
		}
		return pioche();
	}

	public Card[] jouer4(int i, Card carte) {
		ArrayList<Card> tmp = new ArrayList<Card>();
		tmp = hands;
		Card[] cards = jouer3(i, carte);
		if (cards == null)
			return cards;
		Card card = cards[cards.length - 1];
		ArrayList<Card> cars = new ArrayList<Card>();
		cars.addAll(Arrays.asList(cards));

		int nbo = nbNumber(card.getNumber(), hands);
		int nbc = nbColor(card.getColor(), hands);

		if (!depot(card, tmp) || card.is8() || cards.length == handCards.size()) {
			return cards;
		}

		if (!deck.isEmpty() && card.isEquivalent(deck.get(0))) {
			Card c = deck.get(0);
			tmp.add(0, c);
			if (!depot(c, tmp)) {
				pioche();
				return new Card[] { c };
			}
		}

		for (Card c : handCards) {
			if (c.is8()) {
				return new Card[] { c };
			}
		}

		if ((nbo == hands.size() || nbc == hands.size())
				&& !depot(carte, hands)) {
			return pioche();
		}

		return cards;
	}

	public boolean depot(Card carte, ArrayList<Card> liste) {

		ArrayList<Card> lst = new ArrayList<Card>();
		int nbo = occurance(carte, liste).size();
		int nbn = nbNumber("8", liste);

		if ((nbo == liste.size() || nbn == liste.size()) && !liste.isEmpty()
				&& carte.sameNumber(liste.get(0)))
			return true;

		Card cardh = null, cardb = null;
		int k = 0;
		for (; k < liste.size(); k++) {
			if ((carte.isEquivalent(liste.get(k)) && liste.get(k).isDame())) {
				cardb = liste.remove(k);
				break;
			}
		}

		if (k == liste.size())
			return false;

		for (Card c : liste) {
			if (!c.isDame()) {
				cardh = c;
				break;
			}
		}

		Card bh = null;
		for (Card c : liste) {
			if (c.isEquivalent(cardh)) {
				bh = c;
				break;
			}
		}

		if (!lst.contains(cardb))
			lst.add(cardb);

		for (Card c : liste) {
			if (c.isDame() && !lst.contains(c) && !c.equals(bh)) {
				lst.add(c);
			}
		}

		if (!lst.contains(bh))
			lst.add(bh);
		if (!lst.contains(cardh))
			lst.add(cardh);
		liste.remove(cardh);
		for (Card c : liste) {
			if (!lst.contains(c))
				lst.add(c);
		}

		Card[] carts = new Card[lst.size()];
		int j = 0;
		for (Card c : lst) {
			carts[j] = c;
			j++;
		}
		return valideDame(carts, carte);
	}

	public boolean depot(String demande, ArrayList<Card> liste) {

		ArrayList<Card> lst = new ArrayList<Card>();
		int nbn = nbNumber("8", liste);

		if (nbn == liste.size())
			return true;

		Card cardh = null, cardb = null;
		int k = 0;
		for (; k < liste.size(); k++) {
			if ((demande.equals(liste.get(k).getColor()) && liste.get(k)
					.isDame())) {
				cardb = liste.remove(k);
				break;
			}
		}

		if (cardb == null)
			return false;

		for (Card c : liste) {
			if (!c.isDame()) {
				cardh = c;
				break;
			}
		}

		Card bh = null;
		for (Card c : liste) {
			if (c.isEquivalent(cardh)) {
				bh = c;
				break;
			}
		}

		if (!lst.contains(cardb))
			lst.add(cardb);

		for (Card c : liste) {
			if (c.isDame() && !lst.contains(c) && !c.equals(bh)) {
				lst.add(c);
			}
		}

		if (!lst.contains(bh))
			lst.add(bh);
		if (!lst.contains(cardh))
			lst.add(cardh);
		liste.remove(cardh);
		for (Card c : liste) {
			if (!lst.contains(c))
				lst.add(c);
		}

		Card[] carts = new Card[lst.size()];
		int j = 0;
		for (Card c : lst) {
			carts[j] = c;
			j++;
		}
		return valideDame(carts, carts[0]);
	}

	private boolean valideDame(Card[] cartes, Card enCour) {
		if (cartes == null)
			return false;

		Card c = cartes[0];

		if (!enCour.isEquivalent(c))
			return false;

		if (c.isDame()) {
			int i;
			for (i = 1; i < cartes.length && cartes[i].isDame(); i++) {
			}

			if (i == cartes.length) {
				return true;
			}
			if (cartes[i].is8()) {
				for (; i < cartes.length; i++) {
					if (!cartes[i].is8())
						return false;
				}
				return true;
			}
			if (i == cartes.length - 1)
				return (cartes[i - 1].isEquivalent(cartes[i]));
			return false;
		}
		return false;
	}

	public void viderTable() {
		while (!laTable.isEmpty()) {
			laBanque.add(laTable.remove(0));
		}
	}

	@Override
	public void aDeposer(int i, Card[] cartes) {
		super.aDeposer(i, cartes);

		laTable.addAll(Arrays.asList(cartes));

		if (i == joueurClient.getId()) {
			if (handCards.size() == 1) {
				try {
					play("carte");
				} catch (FileNotFoundException ex) {
					Logger.getLogger(JoueurOrdinateur.class.getName()).log(
							Level.SEVERE, null, ex);
				} catch (IOException ex) {
					Logger.getLogger(JoueurOrdinateur.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
			// afficher(mains);
		} else {
			for (int j = 0; j < cartes.length && cartes != null; j++) {
				if (saMain.contains(cartes[j]))
					saMain.remove(cartes[j]);
				else
					saMain.remove(null);
			}
		}

		if (graphic) {
			maFenetre.demandeLabel.setText("");

			// maFenetre.carteTablePane.rafraichir();
			// maFenetre.carteJoueurPane.rafraichir(mains);
			maFenetre.rafraichir();
		}
	}

	@Override
	public void piocher() {
		super.piocher();

		if (graphic)
			maFenetre.rafraichir();
	}

	@Override
	public String demanderCouleur() {
		if (level == 1)
			return demanderCouleur1();
		if (level == 2)
			return demanderCouleur2();
		if (level == 3)
			return demanderCouleur3();
		if (level == 4)
			return demanderCouleur4();
		return demanderCouleur1();
	}

	public String demanderCouleur1() {
		return handCards.get(0).getColor();
	}

	public String demanderCouleur2() {
		String coul[] = { "pique", "coeur", "trefle", "carreau" };
		Card cBanque = null;
		if (!laBanque.isEmpty())
			cBanque = laBanque.get(0);
		ArrayList<String> cou = new ArrayList<String>();
		int count = 0;
		for (int i = 0; i < 4; i++) {
			for (Card c : handCards) {
				if (coul[i].equals(c.getColor()))
					count++;
			}
			for (Card c : laBanque) {
				if (c != null && coul[i].equals(c.getColor()))
					count++;
			}
			for (Card c : laTable) {
				if (coul[i].equals(c.getColor()))
					count++;
			}
			if (count == 8) {
				cou.add(coul[i]);
			}
			count = 0;
		}

		if (cou.isEmpty()) {
			for (int i = 0; i < 4; i++) {
				for (Card c : handCards) {
					if (coul[i].equals(c.getColor()))
						count++;
				}
				for (Card c : laBanque) {
					if (c != null && coul[i].equals(c.getColor()))
						count++;
				}
				for (Card c : laTable) {
					if (coul[i].equals(c.getColor()))
						count++;
				}
				if (count == 7) {
					cou.add(coul[i]);
				}
				count = 0;
			}
		}

		if (cou.isEmpty()) {
			for (int i = 0; i < 4; i++) {
				for (Card c : handCards) {
					if (coul[i].equals(c.getColor()))
						count++;
				}
				for (Card c : laBanque) {
					if (c != null && coul[i].equals(c.getColor()))
						count++;
				}
				for (Card c : laTable) {
					if (coul[i].equals(c.getColor()))
						count++;
				}
				if (count == 6) {
					cou.add(coul[i]);
				}
				count = 0;
			}
		}
		for (String demande : cou) {
			for (Card c : handCards) {
				if (demande.equals(c.getColor()) && cBanque != null
						&& !demande.equals(cBanque.getColor())) {
					return demande;
				}
			}
			if (cBanque != null && !demande.equals(cBanque.getColor())) {
				return demande;
			}
		}
		if (!cou.isEmpty()) {
			return cou.get(0);
		}
		return demanderCouleur1();
	}

	public String demanderCouleur3() {
		String coul[] = { "pique", "coeur", "trefle", "carreau" };
		for (int k = 0; k < 4; k++) {
			String tm = "";
			for (int j = k + 1; j < 4; j++) {
				if (nbColor(coul[k], hands) >= nbColor(coul[j], hands)) {
					tm = coul[k];
					coul[k] = coul[j];
					coul[j] = tm;
				}
			}
		}
		Card cBanque = null;
		if (!deck.isEmpty())
			cBanque = deck.get(0);
		for (int i = 0; i < 4; i++) {
			if (nbColor(coul[i], handCards) >= 1 && cBanque != null
					&& !coul[i].equals(cBanque.getColor())) {
				return coul[i];
			}
		}
		for (int i = 0; i < 4; i++) {
			if (cBanque != null && !coul[i].equals(cBanque.getColor())) {
				return coul[i];
			}
		}
		for (int i = 0; i < 4; i++) {
			if (nbColor(coul[i], handCards) >= 1) {
				return coul[i];
			}
		}

		return coul[0];
	}

	public String demanderCouleur4() {
		String coul[] = { "pique", "coeur", "trefle", "carreau" };
		String demande = demanderCouleur3();
		if (!depot(demande, hands))
			return demande;
		for (int i = 0; i < 4; i++) {
			if (!depot(coul[i], hands) && !deck.isEmpty()
					&& !deck.get(0).getColor().equals(demande)
					&& !deck.get(0).is8())
				return coul[i];
		}
		for (int i = 0; i < 4; i++) {
			if (!depot(coul[i], hands) && !deck.isEmpty() && !deck.get(0).is8())
				return coul[i];
		}

		for (int i = 0; i < 4; i++) {
			if (!depot(coul[i], hands))
				return coul[i];
		}
		for (Card c : hands) {
			if (!c.isDame() && hands.size() > 2)
				return c.getColor();
		}
		return demanderCouleur3();
	}

	public ArrayList<Card> reste() {
		Deck d = new Deck();
		ArrayList<Card> renv = new ArrayList<Card>();
		for (Card c : d) {
			if (!(handCards.contains(c) || laBanque.contains(c) || laTable
					.contains(c)))
				renv.add(c);
		}
		return renv;
	}

	private ArrayList<Card> deck() {
		Deck d = new Deck();
		ArrayList<Card> renv = new ArrayList<Card>();
		for (Card c : d) {
			if (!(handCards.contains(c) || hands.contains(c) || laTable.contains(c)))
				renv.add(c);
		}
		return renv;
	}

	public boolean containsNumber(String numero, ArrayList<Card> lkl) {
		for (Card c : lkl) {
			if (numero.equals(c.getNumber()))
				return true;
		}
		return false;
	}

	public boolean containsColor(String couleur, ArrayList<Card> lkl) {
		for (Card c : lkl) {
			if (couleur.equals(c.getColor()))
				return true;
		}
		return false;
	}

	public static int nbColor(String couleur, ArrayList<Card> lkl) {
		int count = 0;
		for (Card c : lkl) {
			if (couleur.equals(c.getColor()))
				count++;
		}
		return count;
	}

	public static int nbNumber(String numero, ArrayList<Card> lkl) {
		int count = 0;
		for (Card c : lkl) {
			if (numero.equals(c.getNumber()))
				count++;
		}
		return count;
	}

	/**
	 * @param couleur
	 *            the couleur to set
	 */
	@Override
	public void setCouleur(String couleur) {
		super.setCouleur(couleur);
		if (graphic)
			maFenetre.demandeLabel.setText(couleur);
	}

	@Override
	public void setNbCartesBanque(int nbCartesBanque) {
		super.setNbCartesBanque(nbCartesBanque);
		for (int i = 0; i < nbCartesBanque; i++)
			laBanque.add(null);
		for (int j = 0; j < 32 - (nbCartesBanque + handCards.size()); j++)
			saMain.add(null);

		if (graphic)
			maFenetre.banquePane.rafraichir();
	}

	@Override
	public void setCartesMain(Card[] cartes) {
		super.setCartesMain(cartes);

		if (graphic)
			maFenetre.rafraichir();
		// maFenetre.carteJoueurPane.rafraichir(mains);
	}

	@Override
	public boolean demanderDemarrerNouvellePartie() {
		return true;
	}

	// @Override
	public int nbSecJoueur(Card c) {
		int nb = 0;
		int k = handCards.size();
		for (int i = 0; i < k; i++) {
			if (handCards.get(i).sameNumber(c)) {
				nb++;
			}
		}
		return nb;
	}

	@Override
	public void setNbPoints(int nbPoints) {
		points.add(this.nbPoints + nbPoints);
		super.setNbPoints(nbPoints);
	}

	// @Override
	public void rangerList2() {
		int k = handCards.size();
		ArrayList<Card> liste = new ArrayList<Card>();
		for (int p = 0; p < 8; p++) {
			for (int i = 0; i < k; i++) {
				if (handCards.get(i).getPoids() == p) {
					liste.add(handCards.get(i));
				}
			}
		}
		handCards = liste;
	}

	public void rangerList() {
		int k = handCards.size();
		ArrayList<Card> liste = new ArrayList<Card>();
		for (int p = 0; p < 8; p++) {
			for (int i = 0; i < k; i++) {
				if (p >= 5) {
					if (handCards.get(i).getPoids() == p) {
						liste.add(handCards.remove(i));
						k = handCards.size();
					}
				}
			}
		}
		for (int j = 0; j < liste.size(); j++) {
			handCards.add(liste.get(j));
		}
	}

	@Override
	public boolean demanderTerminerPartie() {
		return false;
	}

	// @Override

	public ArrayList<Card> occurance(Card carte, ArrayList<Card> liste) {
		ArrayList<Card> occu = new ArrayList<Card>();
		for (Card c : liste) {
			if (c.sameNumber(carte))
				occu.add(c);
		}
		return occu;
	}

	public void afficher(ArrayList<Card> cards) {
		for (Card c : cards) {
			System.out.print(c);
		}
		System.out.println();
	}

	Timer timer;

	private void attenteAction(ActionEvent ae) {

		this.timer.start();
	}

	private class Attente implements ActionListener {
		Card[] cards = null;
		Card carte;
		int id;

		public Attente(int id, Card carte) {
			this.carte = carte;
			this.id = id;
		}

		public Card[] joue(int id, Card carte) {
			return play(id, carte);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			attenteAction(ae);
			setCards(joue(id, carte));
			Timer timer;
			timer = (Timer) ae.getSource();
			timer.stop();
		}

		public void setCards(Card[] cards) {
			this.cards = cards;
		}

		public Card[] getCards() {
			return cards;
		}
	}
}
