/**
 * 
 */
package graphic;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * @author Mcicheick
 *
 */
public class Bilan extends JTable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1921480487658363864L;
	/**
	 * 
	 */
	ArrayList<String[]> joueurPoints;
	String[] names;
	@SuppressWarnings("rawtypes")
	Class[] types;
	boolean[] canEdit;
	public Bilan() {
		// TODO Auto-generated constructor stub
	}
	
	public Bilan(ArrayList<String[]> joueurPoints){
		this.joueurPoints = joueurPoints;
		int row = 0;
		int clm = 0;
		row = joueurPoints.size();
		if(!joueurPoints.isEmpty()) clm = joueurPoints.get(0).length;
		names = new String[row];
		canEdit = new boolean[row];
		types = new Class[row];
		for(int i=0;i<row;i++){
			names[i] = joueurPoints.get(i)[0];
			canEdit[i] = false;
			types[i] = java.lang.String.class;
		}
		Object objects[][] = new Object[row][clm];
		Object object[][] = new Object[clm][row];
		for(int i=0; i<row;i++){
			for(int j=0;j<joueurPoints.get(i).length;j++){
				objects[i][j] = joueurPoints.get(i)[j];
			}
		}
		
		for(int i=0;i<row;i++){
			for(int j=0;j<clm;j++){
				object[j][i] = objects[i][j];
			}
		}
		
		setModel(new javax.swing.table.DefaultTableModel(object,names){
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@SuppressWarnings("unchecked")
			public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

		
	}
	
	@Override
	public String toString() {
		String s = "";
		for(int i=0;i<joueurPoints.size();i++){
			String st = joueurPoints.get(i)[0];
			for(int j=1;j<joueurPoints.get(i).length;j++){
				st = st+","+joueurPoints.get(i)[j];
			}
			s = s+st+"\n";
		}
		return s;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] p1 = {"Cheick","10","12","35","0"};
		String[] p2 = {"Ousmane","17","92","67","39"};
		String[] p3 = {"Baya","1","9","7","29"};
		String[] p4 = {"Sagara","24","59","37","20"};
		ArrayList<String[]> lists = new ArrayList<String[]>();
		lists.add(p1);
		lists.add(p2);
		lists.add(p3);
		lists.add(p4);
		JFrame frame = new JFrame();
		frame.setSize(500, 600);
		Bilan bilan = new Bilan(lists);
		frame.add(bilan);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		System.out.println(bilan);
	}

}
