/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 25 juin 2011
 */

package graphic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import serveurJeu.GestionnairePartie;
import serveurJeu.Starter;

import commun.Card;
import commun.Cmd;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class MaFenetre extends JFrame {
	private static final long serialVersionUID = 7330251333510373377L;
	private AbstractJoueur abstractJoueur;
	private Thread threadClient;
	private String nom;

	ArrayList<CartePaneGraphic> cartesTable = new ArrayList<CartePaneGraphic>();
	ArrayList<CartePaneGraphic> cartePane = new ArrayList<CartePaneGraphic>();
	ArrayList<CartePaneBanque> cartesBanque = new ArrayList<CartePaneBanque>();
	public int nbJO;
	public BanquePane banquePane;
	public CarteJoueurPane carteJoueurPane;
	public CarteTablePane carteTablePane;
	public javax.swing.JLabel demandeLabel;
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenu editMenu;
	private JMenu affichage;
	private JMenu niveau;
	private JMenu reseauMenu;
	private JMenu bilanMenu;
	private JMenu helpMenu;
	// private JMenuItem mesPoints;
	private JMenuItem bilanMenuItem;
	private JMenuItem mesPointsMenuItem;
	private JMenuItem activerItem;
	private JMenuItem restart;
	private JMenuItem quitterItem;
	private JMenuItem jOrdi;
	private JMenuItem arrangerItem;
	private JMenuItem changerNom;
	private JMenuItem level1;
	private JMenuItem level2;
	private JMenuItem lancerServeur;
	private JMenuItem connectionServeur;
	private JMenuItem deconnection;
	private JMenuItem cmtJouer;
	private JMenuItem help;
	private JMenuItem nbCarte;
	private JMenuItem level3;
	private JMenuItem level4;
	private JMenuItem changeBg;
	static PanePrincipal panePrincipal;
	private JLayeredPane panePrincipal2;
	private JButton passButton;
	private JButton fauteButton;
	public JButton envoyerButton;
	public javax.swing.JLabel tourLabel;
	public javax.swing.JScrollPane jScrollPane1;
	public JPanel messagePane;
	JScrollPane jScrollPane;
	public javax.swing.JTextArea messageText;
	public javax.swing.JTextField envoyerText;
	private JMenuItem lanceServeur;
	private JMenuItem stopServeur;
	public static boolean activer = true;
	JoueurGraphic joueurGraphic;
	private JMenuItem attente;
	private JMenuItem background;

	/**
	 * @param abstractJoueur
	 * @param nom
	 */
	public MaFenetre(AbstractJoueur abstractJoueur, String nom) {
		nbJO = 1;
		setTitle(nom + " - Niveau " + JoueurOrdinateur.level);

		setSize(800, 520);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

		this.abstractJoueur = abstractJoueur;
		this.nom = nom;
		abstractJoueur.setNom(nom);
		threadClient = new Thread(abstractJoueur.getJoueurClient());

		initComponents();

		getRootPane().setDefaultButton(passButton);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - getWidth()) / 2,
				(screen.height - getHeight()) / 2);

		threadClient.start();
	}

	public MaFenetre(String nom) {
		nbJO = 1;
		this.nom = nom;
		setTitle(nom + " - Niveau " + JoueurOrdinateur.level);

		setSize(800, 520);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

		initComponents();

		getRootPane().setDefaultButton(passButton);

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - getWidth()) / 2,
				(screen.height - getHeight()) / 2);
	}

	public MaFenetre(String nom, boolean flag) {
		this.nom = nom;
		setTitle(nom);
		initComponents2();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screen.width - getWidth()) / 2,
				(screen.height - getHeight()) / 2);

		Starter starter = null;
		try {
			starter = new Starter();
			starter.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

		byte[] addr = { (byte) 127, (byte) 0, (byte) 0, (byte) 1 };
		nom = "Utilisateur";
		try {
			JoueurClient joueurClient = new JoueurClient(addr,
					starter.getPort());

			joueurGraphic = new JoueurGraphic(joueurClient);
			MaFenetre fenetre = new MaFenetre(joueurGraphic, nom);
			joueurGraphic.setMaFenetre(fenetre);
			joueurGraphic.getMaFenetre().setVisible(true);

			joueurGraphic.start();
			dispose();
		} catch (UnknownHostException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	private void initComponents() {
		if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
			setMacMenuBar();
			setSize(800, 480);
		}

		panePrincipal = new PanePrincipal();
		panePrincipal.setLayout(null);
		panePrincipal.setSize(getWidth(), getHeight());
		panePrincipal.setLocation(0, 0);

		ajouterMenuBar();
		ajouterDemandeLabel();
		ajouterTourLabel();
		ajouterBanquePane();
		ajouterPanePrincipal2();
		ajouterMessagePane();

		add(panePrincipal);
	}

	private void initComponents2() {
		if (System.getProperty("os.name").toLowerCase().startsWith("mac")) {
			setMacMenuBar();
			setSize(800, 480);
		}

		panePrincipal = new PanePrincipal();
		panePrincipal.setLayout(null);
		panePrincipal.setSize(getWidth(), getHeight());
		panePrincipal.setLocation(0, 0);

		ajouterMenuBar();
		ajouterDemandeLabel();
		ajouterTourLabel();

		add(panePrincipal);
	}

	private void ajouterPanePrincipal2() {
		panePrincipal2 = new JLayeredPane();
		panePrincipal2.setSize(500, (180 * 2 + 45));
		panePrincipal2.setLocation(10, 50);
		panePrincipal2.setBackground(Color.CYAN);
		ajouterCarteTablePane();
		ajouterCarteJoueurPane();

		panePrincipal.add(panePrincipal2);
	}

	private void ajouterBanquePane() {
		banquePane = new BanquePane(abstractJoueur);
		// banquePane.setBorder(BorderFactory.createTitledBorder("Banque"));
		banquePane.setLayout(null);
		banquePane.setSize(240, 180);
		banquePane.setLocation(540, 10);

		panePrincipal.add(banquePane, -1);
	}

	private void ajouterCarteJoueurPane() {

		carteJoueurPane = new CarteJoueurPane();
		// carteJoueurPane.setBorder(BorderFactory.createTitledBorder("Vos cartes"));
		carteJoueurPane.setLayout(null);
		carteJoueurPane.setSize(500, 180);
		carteJoueurPane.setLocation(0, 180);

		passButton = new JButton("Pass");
		passButton.setSize(80, 30);
		int x = (carteJoueurPane.getWidth() - passButton.getWidth()) / 2;
		x += carteJoueurPane.getX();

		int y = carteJoueurPane.getY() + carteJoueurPane.getHeight() + 10;
		passButton.setLocation(x, y);
		passButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (abstractJoueur.aMain()) {
					abstractJoueur.passerTour();
				}
			}
		});
		panePrincipal2.add(passButton);

		fauteButton = new JButton("Faute");
		fauteButton.setSize(80, 30);

		fauteButton.setLocation(x - 80, y);
		fauteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// if(GestionnairePartie.faute){
				// GestionnairePartie.faute = false;
				// abstractJoueur.faute();
				// }
			}
		});
		panePrincipal2.add(fauteButton);

		dessinerCartesMain();

		panePrincipal2.add(carteJoueurPane, new Integer(-1));
	}

	private void ajouterMenuBar() {
		menuBar = new JMenuBar();

		// file menu
		fileMenu = new JMenu("File");

		activerItem = new JMenuItem("Désactiver le son");
		activerItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String text;
				JMenuItem activerItem;

				activerItem = (JMenuItem) e.getSource();
				text = (activer) ? "Activer le son" : "Désactiver le son";
				activerItem.setText(text);
				activer = !activer;
			}
		});
		fileMenu.add(activerItem);

		restart = new JMenuItem("Recommencer");
		restart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				GestionnairePartie.terminerPartie();
				abstractJoueur.passerTour();
			}
		});
		fileMenu.add(restart);

		quitterItem = new JMenuItem("Quitter");
		quitterItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(quitterItem);

		menuBar.add(fileMenu);

		// edit menu
		editMenu = new JMenu("Edit");

		jOrdi = new JMenuItem("Lancer un joueur ordinateur");
		jOrdi.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				jOrdiActionListener();
			}
		});
		editMenu.add(jOrdi);

		arrangerItem = new JMenuItem("Arranger");
		arrangerItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((JoueurGraphic) abstractJoueur).rangerList();
				rafraichir();
			}
		});
		editMenu.add(arrangerItem);

		nbCarte = new JMenuItem("Nombre de cartes");
		nbCarte.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				String nbs = JOptionPane.showInputDialog(null,
						"Donnez le nombre de cartes");
				try {
					int nb = Integer.parseInt(nbs);
					GestionnairePartie.setNbCartes(nb);
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		editMenu.add(nbCarte);

		attente = new JMenuItem("Temps d'attente");
		attente.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String nbs = JOptionPane.showInputDialog(null,
						"Donnez votre temps d'attente en seconde");
				try {
					double nb = Double.parseDouble(nbs);
					JoueurOrdinateur.mill = (int) (nb * 1000);
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, ex.getMessage());
				}
			}
		});
		editMenu.add(attente);

		changerNom = new JMenuItem("Changer nom du joueur");
		changerNom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				changerNomActionListener();
			}
		});
		editMenu.add(changerNom);

		niveau = new JMenu("Niveau");
		level1 = new JMenuItem("Niveau 1");
		level1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				JoueurOrdinateur.level = 1;
				setTitle(nom + " - Niveau 1");
			}
		});
		niveau.add(level1);

		level2 = new JMenuItem("Niveau 2");
		level2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				JoueurOrdinateur.level = 2;
				setTitle(nom + " - Niveau 2");
			}
		});
		niveau.add(level2);

		level3 = new JMenuItem("Niveau 3");
		level3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				JoueurOrdinateur.level = 3;
				setTitle(nom + " - Niveau 3");
			}
		});
		niveau.add(level3);

		level4 = new JMenuItem("Niveau 4");
		level4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				JoueurOrdinateur.level = 4;
				setTitle(nom + " - Niveau 4");
			}
		});
		niveau.add(level4);

		// affichage
		affichage = new JMenu("Affichage");
		changeBg = new JMenuItem("Choisir une image de fond");
		changeBg.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser;
				fileChooser = new JFileChooser();
				fileChooser.setVisible(true);
				int rps = JFileChooser.APPROVE_OPTION;
				if (rps == fileChooser.showOpenDialog(fileChooser)) {
					Image image = Toolkit.getDefaultToolkit().getImage(
							fileChooser.getSelectedFile().getAbsolutePath());
					panePrincipal.rafraichir(image);
				}
			}
		});
		affichage.add(changeBg);

		background = new JMenuItem("Changer image de fond");
		background.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				backgroundAction();
			}
		});
		affichage.add(background);

		// reseau menu
		reseauMenu = new JMenu("Réseau");

		lancerServeur = new JMenuItem("Lancer le serveur");
		lancerServeur.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				lancerServeurActionPerformed();
			}
		});
		reseauMenu.add(lancerServeur);

		lanceServeur = new JMenuItem("Lancer le serveur sur 7000");
		lanceServeur.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					new Starter().start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		reseauMenu.add(lanceServeur);

		stopServeur = new JMenuItem("Stopper le serveur sur 7000");
		stopServeur.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					new Starter().stop();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		reseauMenu.add(stopServeur);

		connectionServeur = new JMenuItem("Connection au serveur");
		connectionServeur
				.addActionListener(new java.awt.event.ActionListener() {
					@Override
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						connectionServeurActionPerformed(evt);
					}
				});
		reseauMenu.add(connectionServeur);

		deconnection = new JMenuItem("Déconnection");
		deconnection.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				deconnectionActionPerformed(evt);
			}
		});
		reseauMenu.add(deconnection);

		bilanMenu = new JMenu("Bilan");

		mesPointsMenuItem = new JMenuItem("Mes points");
		mesPointsMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Vos points "
						+ abstractJoueur.nbPoints);
			}
		});
		bilanMenu.add(mesPointsMenuItem);

		bilanMenuItem = new JMenuItem("Bilan");
		bilanMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				showBilan();
			}
		});
		bilanMenu.add(bilanMenuItem);

		helpMenu = new JMenu("Help");

		help = new JMenuItem("A propos de 151");
		help.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				helpAction();
			}
		});
		helpMenu.add(help);

		cmtJouer = new JMenuItem("Comment jouer");
		cmtJouer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmtJouerAction();
			}
		});
		helpMenu.add(cmtJouer);

		menuBar.add(editMenu);
		menuBar.add(affichage);
		menuBar.add(niveau);
		menuBar.add(bilanMenu);
		menuBar.add(reseauMenu);
		menuBar.add(helpMenu);

		setJMenuBar(menuBar);

	}

	private void ajouterCarteTablePane() {

		carteTablePane = new CarteTablePane();
		// carteTablePane.setBorder(BorderFactory.createTitledBorder("Table"));
		carteTablePane.setLayout(null);
		carteTablePane.setSize(500, 180);
		carteTablePane.setLocation(0, 0);

		dessinerCartesTable();

		panePrincipal2.add(carteTablePane, new Integer(-1));
	}

	private void ajouterMessagePane() {
		messagePane = new JPanel();
		messageText = new JTextArea();
		jScrollPane = new JScrollPane();
		// messagePane.setBorder(BorderFactory.createTitledBorder("Tchat"));
		messagePane.setLayout(null);
		messagePane.setSize(240, 255);
		messagePane.setLocation(540, 200);

		messageText.setSize(230, 200);
		messageText.setLocation(3, 15);
		messageText.setColumns(15);
		messageText.setRows(10);
		messageText.setEditable(false);
		messageText.setForeground(Color.BLUE);

		jScrollPane.setViewportView(messageText);
		jScrollPane.setSize(231, 210);
		jScrollPane.setAutoscrolls(true);
		jScrollPane.setLocation(3, 10);

		// messageText.setBackground(new Color(255, 255, 160));

		envoyerText = new JTextField();
		envoyerText.setSize(130, 30);
		envoyerText.setLocation(messageText.getX(),
				messageText.getHeight() + 20);
		// envoyerText.setBackground(new Color(255, 255, 160));
		envoyerText.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				envoyerTextKeyTyped(evt);
			}
		});

		envoyerButton = new JButton("Envoyer");
		envoyerButton.setSize(100, 30);
		envoyerButton.setLocation(envoyerText.getWidth() + 5,
				envoyerText.getY());

		envoyerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String oldM = messageText.getText();
				String newM = envoyerText.getText();

				if (!newM.isEmpty() && !oldM.isEmpty())
					messageText.setText(oldM + "\n" + abstractJoueur.getNom()
							+ ": " + newM);
				else if (!newM.isEmpty()) {
					messageText.setText(abstractJoueur.getNom() + ": " + newM);
					abstractJoueur.envoyerMessage(newM);
				}

				envoyerText.setText("");

				getRootPane().setDefaultButton(passButton);
			}
		});

		messagePane.add(jScrollPane);
		messagePane.add(envoyerText);
		messagePane.add(envoyerButton);

		panePrincipal.add(messagePane);
	}

	private void ajouterDemandeLabel() {
		demandeLabel = new JLabel("");
		demandeLabel.setSize(100, 30);
		demandeLabel.setLocation(20, 10);

		panePrincipal.add(demandeLabel);
	}

	private void ajouterTourLabel() {
		tourLabel = new JLabel("");
		tourLabel.setSize(100, 30);
		tourLabel.setLocation(10 + (500 - tourLabel.getWidth()), 10);

		panePrincipal.add(tourLabel);
	}

	private void setMacMenuBar() {
		System.setProperty("apple.laf.useScreenMenuBar", "true");
	}

	public void dessinerCartesBanque() {
		int x = banquePane.getX();
		int y = banquePane.getY();

		if (!cartesBanque.isEmpty()) {

			for (CartePaneBanque cartepane : cartesBanque) {
				panePrincipal.remove(cartepane);
			}

			cartesBanque.clear();
		}

		int nbBanque = abstractJoueur.getNbCartesBanque();

		for (int i = 0; i < nbBanque; i++) {
			cartesBanque.add(new CartePaneBanque(new Card("", ""),
					abstractJoueur));
		}

		for (int i = 0; i < cartesBanque.size(); i++) {
			CartePaneBanque carte = cartesBanque.get(i);
			carte.setLocation(x
					+ (banquePane.getHeight() - 2 * nbBanque - Card.X / 2) / 2
					+ i * 2, y + (banquePane.getHeight() - Card.Y) / 2);
			panePrincipal.add(carte, i);
		}

	}

	public void dessinerCartesTable() {
		if (!cartesTable.isEmpty()) {

			for (CartePaneGraphic cartepane : cartesTable) {
				panePrincipal2.remove(cartepane);
			}

			cartesTable.clear();
		}

		int t = (carteTablePane.getWidth() - Card.X) / 30;

		int index = Math.max(0, abstractJoueur.getTables().size() - t);

		for (int i = index; i < abstractJoueur.getTables().size(); i++) {
			cartesTable.add(new CartePaneGraphic(abstractJoueur.getTables()
					.get(i), abstractJoueur));
		}

		for (int i = 0; i < cartesTable.size(); i++) {

			CartePaneGraphic carte = cartesTable.get(i);
			carte.setClickable(false);
			carte.setLocation(10 + i * 30,
					(carteTablePane.getHeight() - Card.Y) / 2);
			panePrincipal2.add(carte, new Integer(i));

		}
	}

	public void dessinerCartesMain() {
		ArrayList<Card> cartes = abstractJoueur.getCartesMain();

		repaint();

		if (!cartePane.isEmpty()) {
			for (CartePaneGraphic cartepane : cartePane) {
				panePrincipal2.remove(cartepane);
			}
			cartePane.clear();
		}

		for (int i = 0; i < cartes.size(); i++) {
			cartePane.add(new CartePaneGraphic(cartes.get(i), abstractJoueur));
		}

		for (int i = 0; i < cartePane.size(); i++) {

			CartePaneGraphic carte = cartePane.get(i);
			carte.setClickable(true);
			carte.setLocation(10 + i * 30, carteTablePane.getHeight() + 5
					+ (carteJoueurPane.getHeight() - Card.Y) / 2);
			panePrincipal2.add(carte, new Integer(i));

		}
	}

	private void helpAction() {
		Help helpText = new Help(this, "About", true);
		helpText.setVisible(true);
	}

	private void cmtJouerAction() {
		Help helpText = new Help(this, true);
		helpText.setVisible(true);
	}

	private void envoyerTextKeyTyped(java.awt.event.KeyEvent evt) {
		if (evt.getKeyChar() != 10)
			getRootPane().setDefaultButton(envoyerButton);
	}

	public void rafraichir() {
		repaint();
		// dessinerCartesBanque();
		dessinerCartesTable();
		dessinerCartesMain();
	}

	public void changerNomActionListener() {
		nom = JOptionPane.showInputDialog(this, "Donnez le nouveau nom");
		abstractJoueur.setNom(nom);
		setTitle(nom + " - Niveau " + JoueurOrdinateur.level);
		try {
			abstractJoueur.getJoueurClient().setNom(nom);
		} catch (IOException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	public void jOrdiActionListener() {
		try {
			byte[] addr = { 127, 0, 0, 1 };
			int port = 7000;

			JoueurClient joueurClientOrdi = new JoueurClient(addr, port);

			JoueurOrdinateur joueurOrdinateur = new JoueurOrdinateur(
					joueurClientOrdi);
			if (joueurOrdinateur.graphic) {
				joueurOrdinateur.setMaFenetre(new MaFenetre(joueurOrdinateur,
						"Ordinateur " + nbJO));
				joueurOrdinateur.getMaFenetre().setVisible(true);
			}
			joueurOrdinateur.setNom("Ordinateur " + nbJO);
			nbJO++;
			new Thread(joueurOrdinateur.getJoueurClient()).start();

		} catch (UnknownHostException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (IOException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	private void lancerServeurActionPerformed() {
		new ServeurInfo(this, true).setVisible(true);
	}

	private void connectionServeurActionPerformed(java.awt.event.ActionEvent evt) {
		new ServeurConnection(this, true).setVisible(true);
	}

	private void deconnectionActionPerformed(ActionEvent evt) {
		try {
			abstractJoueur.getJoueurClient().deconnection();
			((JoueurGraphic) abstractJoueur).stop();
		} catch (IOException ex) {
			Logger.getLogger(MaFenetre.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	private void showBilan() {
		try {
			abstractJoueur.getJoueurClient().bilan();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JDialog bilan = new JDialog(this, true);
		bilan.setSize(getWidth() / 2, getHeight() / 2);
		bilan.setLocation(getX() + (getWidth() - bilan.getWidth()) / 2, getY()
				+ (getHeight() - bilan.getHeight()) / 2);
		// System.out.println(abstractJoueur.getBilan()==null);
		bilan.add(abstractJoueur.bilan);
		bilan.setVisible(true);

	}

	private class BackGround extends JDialog {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		PanePrincipal panep;
		PanePrincipal pane1;
		PanePrincipal pane2;
		PanePrincipal pane3;
		PanePrincipal pane4;
		PanePrincipal pane5;
		PanePrincipal pane6;
		PanePrincipal pane7;
		PanePrincipal pane8;

		public BackGround(final MaFenetre frame, boolean bln) {
			super(frame, bln);
			setTitle("Choix de couleur");
			setSize(365, 185);
			Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
			setLocation((screen.width - getWidth()) / 2,
					(screen.height - getHeight()) / 2);

			panep = new PanePrincipal(MaFenetre.panePrincipal.getImage());
			panep.setSize(365, 160);
			panep.setLayout(null);

			pane1 = new PanePrincipal("../" + Cmd.cartePath + "bg1" + Cmd.BE);
			pane1.setSize(365, 20);
			pane1.setLocation(0, 0);
			pane1.setLayout(null);
			pane1.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane1.getImage());
					frame.tourLabel.setForeground(Color.black);
					frame.demandeLabel.setForeground(Color.black);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane1);

			pane2 = new PanePrincipal(Cmd.cartePath + "bg2" + Cmd.BE);
			pane2.setSize(365, 20);
			pane2.setLocation(0, 20);
			pane2.setLayout(null);
			pane2.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane2.getImage());
					frame.tourLabel.setForeground(Color.black);
					frame.demandeLabel.setForeground(Color.black);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane2);

			pane3 = new PanePrincipal(Cmd.cartePath + "bg3" + Cmd.BE);
			pane3.setSize(365, 20);
			pane3.setLocation(0, 40);
			pane3.setLayout(null);
			pane3.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane3.getImage());
					frame.tourLabel.setForeground(Color.black);
					frame.demandeLabel.setForeground(Color.black);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane3);

			pane4 = new PanePrincipal(Cmd.cartePath + "bg4" + Cmd.BE);
			pane4.setSize(365, 20);
			pane4.setLocation(0, 60);
			pane4.setLayout(null);
			pane4.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane4.getImage());
					frame.tourLabel.setForeground(Color.black);
					frame.demandeLabel.setForeground(Color.black);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane4);

			pane5 = new PanePrincipal(Cmd.cartePath + "bg5" + Cmd.BE);
			pane5.setSize(365, 20);
			pane5.setLocation(0, 80);
			pane5.setLayout(null);
			pane5.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane5.getImage());
					frame.tourLabel.setForeground(Color.black);
					frame.demandeLabel.setForeground(Color.black);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane5);

			pane6 = new PanePrincipal(Cmd.cartePath + "bg6" + Cmd.BE);
			pane6.setSize(365, 20);
			pane6.setLocation(0, 100);
			pane6.setLayout(null);
			pane6.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane6.getImage());
					frame.tourLabel.setForeground(Color.CYAN);
					frame.demandeLabel.setForeground(Color.cyan);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane6);

			pane7 = new PanePrincipal(Cmd.cartePath + "bg7" + Cmd.BE);
			pane7.setSize(365, 20);
			pane7.setLocation(0, 120);
			pane7.setLayout(null);
			pane7.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane7.getImage());
					frame.tourLabel.setForeground(Color.CYAN);
					frame.demandeLabel.setForeground(Color.cyan);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane7);

			pane8 = new PanePrincipal(Cmd.cartePath + "bg8" + Cmd.BE);
			pane8.setSize(365, 20);
			pane8.setLocation(0, 140);
			pane8.setLayout(null);
			pane8.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent me) {
					panePrincipal.rafraichir(pane8.getImage());
					frame.tourLabel.setForeground(Color.CYAN);
					frame.demandeLabel.setForeground(Color.cyan);
					dispose();
				}

				@Override
				public void mousePressed(MouseEvent me) {
				}

				@Override
				public void mouseReleased(MouseEvent me) {
				}

				@Override
				public void mouseEntered(MouseEvent me) {
				}

				@Override
				public void mouseExited(MouseEvent me) {
				}
			});
			panep.add(pane8);

			add(panep);

			setResizable(false);

		}

	}

	public void backgroundAction() {
		BackGround bg = new BackGround(this, true);
		bg.setLocation(getX() + (getWidth() - bg.getWidth()) / 2, getY()
				+ (getHeight() - bg.getHeight()) / 2);
		bg.setVisible(true);
	}

	// End of variables declaration

	public PanePrincipal getPanePrincipal() {
		return panePrincipal;
	}

}
