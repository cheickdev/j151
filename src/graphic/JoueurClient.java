/**
 *
 * author: TRAOR&Eacute; Bilal
 * version: 0.0.1
 * date: 7 juin 2011
 */

package graphic;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import serveurJeu.Starter;


import commun.Card;
import commun.Cmd;
import crypto.Crypto;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class JoueurClient implements InterfaceJoueurClient, Runnable
{
    private static final boolean PRINT = false;
    private final Socket socket;
    private int id;
    private BufferedReader istream;
    private DataOutputStream ostream;
    
    private AbstractJoueur joueur;
    
    public Thread thread = null;
    
    private byte[] addr;
    private int port;
    
    JoueurGraphic joueurGraphic;
    
    
    /**
     * @param addr 
     * @param port 
     * @throws IOException 
     * @throws UnknownHostException 
     */
    public JoueurClient(byte[] addr, int port)
        throws UnknownHostException, IOException
    {
        socket = new Socket(InetAddress.getByAddress(addr), port);
        istream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ostream = new DataOutputStream(socket.getOutputStream());
        thread = new Thread(this);
    }

    private void print(String msg)
    {
        if (PRINT)
            System.out.print(msg);
    }

    private void println(String msg)
    {
        print(msg + "\n");
    }

    public byte[] getAddr() {
        return addr;
    }

    public void setAddr(byte[] addr) {
        this.addr = addr;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}
	
	public JoueurGraphic getJoueurGraphic() {
		return joueurGraphic;
	}

	public void setJoueurGraphic(JoueurGraphic joueurGraphic) {
		this.joueurGraphic = joueurGraphic;
	}

	public void start(){
		thread.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop(){
		thread.stop();
	}

	public void recevoirMessage() throws IOException
    {
		//System.out.println("On a reçu un message!");
        String ligne = istream.readLine();
        String nom = ligne;
        ligne = istream.readLine();
        int taille = Integer.parseInt(ligne);
        
        StringBuilder message = new StringBuilder(taille);
        int c = istream.read();
        while (c != -1 && taille > 0) {
        	//System.out.println((char) c);
            message.append((char) c);
            taille--;
            c = istream.read();
        }
        ligne = istream.readLine();//FIN_CMD_N
        
//        String nom = getJoueurNom(id);
        //System.out.println(message.toString());
        String msg = Crypto.decoder(message.toString());
        //System.out.println(msg);
        joueur.setMessage(Crypto.decoder(nom) + ": " + msg);
    }

    @Override
    public void aDemanderCouleur() throws IOException
    {
        String couleur;
        
        istream.readLine();//id
        couleur = istream.readLine();
        istream.readLine();//FIN_CMD_N
        
        joueur.setCouleur(couleur);
    }

    @Override
    public void aDeposer() throws IOException
    {
        Card[] cartes;
        int i, nbCartes;
        String ligne;
        
        i = Integer.parseInt(istream.readLine());
        nbCartes = Integer.parseInt(istream.readLine());
        cartes = new Card[nbCartes];
        
        ligne = istream.readLine();
        for (int j = 0; j < nbCartes; j++) {
            cartes[j] = Card.creerCarte(ligne);
            print(" " + cartes[j]);
            ligne = istream.readLine();
        }
        
        joueur.aDeposer(i, cartes);
        println("");
    }

    @Override
    public void aGagner() throws IOException
    {
        int i;
        
        i = Integer.parseInt(istream.readLine());
        istream.readLine();//FIN_CMD_N
        
        joueur.aGagner(i);
    }
    
    @Override
    public void aGagner2() throws IOException
    {
        String nom;
        
        nom = istream.readLine();
        istream.readLine();//FIN_CMD_N
        
        joueur.aGagner(nom);
    }

    @Override
    public void aPiocher() throws IOException
    {
        Card[] cartes;
        int i, nbCartes;
        String ligne;
        
        cartes = null;
        i = Integer.parseInt(istream.readLine());
        nbCartes = Integer.parseInt(istream.readLine());
        ligne = istream.readLine();
        if (id == i) {
            cartes = new Card[nbCartes];
            
            for (int j = 0; j < nbCartes; j++) {
                cartes[j] = Card.creerCarte(ligne);
                print(" " + cartes[j]);
                ligne = istream.readLine();
            }
        }
        
        joueur.aPiocher(i, cartes, nbCartes);
        println("");
    }

    @Override
    public void demanderCouleur() throws IOException
    {
        istream.readLine();//FIN_CMD_N
        String couleur = joueur.demanderCouleur();
        
        ostream.writeBytes(Cmd.DEMANDER_COULEUR + "\n");
        ostream.writeBytes(couleur + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    @Override
    public void demanderDemarrerNouvellePartie() throws IOException
    {
        istream.readLine();//FIN_CMD_N
        boolean ok = joueur.demanderDemarrerNouvellePartie();
        
        ostream.writeBytes(Cmd.DEMANDER_DEMARRER_NOUVELLE_PARTIE + "\n");
        ostream.writeBytes(ok + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    @Override
    public void demanderTerminerPartie() throws IOException
    {
        istream.readLine();//FIN_CMD_N
        boolean ok = joueur.demanderTerminerPartie();
        
        ostream.writeBytes(Cmd.DEMANDER_TERMINER_PARTIE + "\n");
        ostream.writeBytes(ok + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    /**
     * @throws IOException 
     */
    public void envoyerInfo() throws IOException
    {
        setNom(joueur.getNom());
    }

    @Override
    public void envoyerMessage(String message) throws IOException
    {
    	message = Crypto.coder(message);
        ostream.writeBytes(Cmd.MESSAGE + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(message.length() + "\n");
        ostream.writeBytes(message);
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    @Override
    public int getId()
    {
        return id;
    }

    /**
     * @return the joueur
     */
    public AbstractJoueur getJoueur()
    {
        return joueur;
    }
    
    @Override
    public void jouer() throws IOException
    {
        int i;
        Card carte, cartes[];
        String ligne;
        
        // Lire la carte jouée
        i = Integer.parseInt(istream.readLine());
        ligne = istream.readLine();
        carte = null;
        if (!ligne.equals("null"))
            carte = Card.creerCarte(ligne);
        istream.readLine();//FIN_CMD_N
        
        // Recuperer les cartes jouées
        cartes = joueur.jouer(i, carte);
        
        
        if (cartes == null)
            return;
        
        // Envoye les cartes au serveur
        ostream.writeBytes(Cmd.JOUER + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(cartes.length + "\n");
        for (int j=0; j < cartes.length; j++) {
            ostream.writeBytes(cartes[j] + "\n");
            print(" " + cartes[j]);
        }
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    @Override
    public void piocher() throws IOException
    {
        ostream.writeBytes(Cmd.PIOCHER + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    @Override
    public void piocher(int nbCarte) throws IOException
    {
        ostream.writeBytes(Cmd.PIOCHER + "\n");
        ostream.writeBytes(nbCarte+"\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }


    @Override
    public void points() throws IOException
    {
        int nbPoints = Integer.parseInt(istream.readLine());
        istream.readLine();//FIN_CMD_N
        
        joueur.setNbPoints(nbPoints);
    }
    
    @Override
    public void run()
    {
        String ligne;
        
        try
        {
            envoyerInfo();
            
            ligne = istream.readLine();
            while (ligne != null) {
                if (ligne.equals(Cmd.A_DEMANDER_COULEUR)) {
                    aDemanderCouleur();
                    
                } else if (ligne.equals(Cmd.A_DEPOSER)) {
                    aDeposer();
                    
                } else if (ligne.equals(Cmd.A_GAGNER)) {
                    //aGagner();
                    aGagner2();
                    
                } else if (ligne.equals(Cmd.A_PIOCHER)) {
                    aPiocher();
                    
                } else if (ligne.equals(Cmd.DEMANDER_COULEUR)) {
                    demanderCouleur();
                    
                } else if (ligne.equals(Cmd.DEMANDER_DEMARRER_NOUVELLE_PARTIE)) {
                    demanderDemarrerNouvellePartie();
                    
                } else if (ligne.equals(Cmd.DEMANDER_TERMINER_PARTIE)) {
                    demanderTerminerPartie();
                    
                } else if (ligne.equals(Cmd.JOUER)) {
                    jouer();
                    
                } else if (ligne.equals(Cmd.PIOCHER)) {
                    // rien a faire
                } else if (ligne.equals(Cmd.POINTS)) {
                    points();
                    
                } else if (ligne.equals(Cmd.SET_CARTES)) {
                    setCartesMain();
                    
                } else if (ligne.equals(Cmd.SET_NB_CARTES_BANQUE)) {
                    setCartesBanque();
                    
                } else if (ligne.equals(Cmd.SET_ID)) {
                    setId();
                    
                } else if (ligne.equals(Cmd.MESSAGE)) {
                    recevoirMessage();
                    
                } else if(ligne.equals(Cmd.FAUTE)){
                	rcvFaute();
                } else if(ligne.equals(Cmd.BILAN)){
                	rcvBilan();
                }
                ligne = istream.readLine();
            }
            
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private void rcvFaute() throws IOException {
        int nbCarte = Integer.parseInt(istream.readLine());
        istream.readLine();//FIN_CMD_N
		joueur.play("faute");
		joueur.faute = true;
		piocher(nbCarte);
	}

	@Override
    public String getJoueurNom(int id) throws IOException
    {
        ostream.writeBytes(Cmd.GET_NOM + "\n");
        ostream.writeBytes(id + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
        
        String ligne = istream.readLine();
        while (ligne != null && !ligne.equals(Cmd.GET_NOM)) {
            ligne = istream.readLine();
        }
        ligne = istream.readLine();
        istream.readLine();//FIN_CMD_N
        
        return Crypto.decoder(ligne);
    }
    
    @Override
    public void setCartesBanque() throws IOException
    {
        int nbCartesBanque = Integer.parseInt(istream.readLine());
        istream.readLine();//FIN_CMD_N
        
        joueur.setNbCartesBanque(nbCartesBanque);
    }

    @Override
    public void setCartesMain() throws IOException
    {
        int i, nbCartes;
        Card[] cartes;
        String ligne;
        
        nbCartes = Integer.parseInt(istream.readLine());
        cartes = new Card[nbCartes];
        i = 0;
        ligne = istream.readLine();
        while (ligne != null && i < nbCartes) {
            cartes[i] = Card.creerCarte(ligne);
            print(" " + cartes[i]);
            i++;
            ligne = istream.readLine();
        }
        
        joueur.setCartesMain(cartes);
        println("");
    }
    
    /**
     * @param joueur the joueur to set
     */
    public void setJoueur(AbstractJoueur joueur)
    {
        this.joueur = joueur;
    }

    @Override
    public void setId() throws IOException
    {
        id = Integer.parseInt(istream.readLine());
        istream.readLine();//FIN_CMD_N
    }

    @Override
    public void passerTour() throws IOException
    {
        ostream.writeBytes(Cmd.PASSER_TOUR + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    @Override
    public void faute() throws IOException
    {
        ostream.writeBytes(Cmd.FAUTE + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }

    @Override
    public void setNom(String nom) throws IOException
    {
        ostream.writeBytes(Cmd.SET_NOM + "\n");
        ostream.writeBytes(Crypto.coder(nom) + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
    }
    
    public void deconnection() throws IOException{
        ostream.writeBytes(Cmd.DECONNECTION + "\n");
        ostream.writeBytes(Cmd.FIN_CMD_N);
        stop();
    }
    
    public void bilan() throws IOException{
    	ostream.writeBytes(Cmd.BILAN + "\n");
    	ostream.writeBytes(Cmd.FIN_CMD_N);
    	
    	if(joueur.aMain) rcvBilan();
    }
    
    public void rcvBilan() throws IOException{
    	ArrayList<String[]> list = new ArrayList<String[]>();
    	String ligne = istream.readLine();
    	if(ligne.equals(Cmd.BILAN)) ligne = istream.readLine();
    	//int i=0;
    	while(ligne != null && !ligne.equals(Cmd.FIN_CMD)){
    		String[] row = ligne.split(",");
    		list.add(row);
    		//System.out.println(getId()+" "+i+" "+ligne);
    		ligne = istream.readLine();
    		//i++;
    	}
    	joueur.setBilan(list);
    }
}
