package crypto;

public class Crypto {

	/**
	 * @param args
	 */
	
	public static String coder(String message){
		if(message == null) return null;
		String messageCrypte = ((int) message.charAt(0))+"";
		for(int i=1; i<message.length();i++){
			messageCrypte = messageCrypte +" "+((int)message.charAt(i));
		}
		return messageCrypte;
	}
	
	public static String decoder(String message){
		if(message == null) return null;
		String messageBytes[] = message.split(" ");
		int byt = Integer.parseInt(messageBytes[0]);
		String messageDecrypte = ((char)byt)+"";
		for(int i=1;i<messageBytes.length;i++){
			byt = Integer.parseInt(messageBytes[i]);
			messageDecrypte = messageDecrypte + ((char)byt);
		}
		return messageDecrypte;
	}
	
	public static void main(String[] args) {
		System.out.println(Crypto.decoder("79 114 100 105 110 97 116 101 117 114 32 49"));
	}

}
