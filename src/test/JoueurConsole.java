/**
 *
 * author: TRAOR&Eacute; Bilal version: 0.0.1 date: 9 juin 2011
 */
package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.UnknownHostException;


import commun.Card;

import graphic.AbstractJoueur;
import graphic.JoueurClient;

/**
 * @author TRAOR&Eacute; Bilal
 * @version 0.0.1
 */
public class JoueurConsole extends AbstractJoueur {

    protected BufferedReader istream;
    protected PrintStream ostream;

    /**
     * @param joueurClient
     */
    public JoueurConsole(JoueurClient joueurClient) {
        super(joueurClient);

        istream = new BufferedReader(new InputStreamReader(System.in));
        ostream = System.out;
    }

    @Override
    public void aGagner(int i) {
        super.aGagner(i);

        if (i == joueurClient.getId()) {
            ostream.print("Vous avez gagne\n");
        } else {
            ostream.print(i + " a gagne\n");
        }

    }

    @Override
    public void setNbPoints(int nbPoints) {
        points.add(this.nbPoints + nbPoints);
        super.setNbPoints(nbPoints);
    }

    @Override
    public String demanderCouleur() {
        try {
            ostream.print("\nQuelle couleur voulez-vous demandee:");
            ostream.print("\n");
            for (int i = 0; i < Card.listeCouleur.length; i++) {
                ostream.print(" " + (i + 1) + "=" + Card.listeCouleur[i]);
            }
            ostream.print("\nCouleur:");
            int j = Integer.parseInt(istream.readLine());
            j--;
            String couleur = Card.listeCouleur[j];
            return couleur;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean demanderDemarrerNouvellePartie() {
        try {
            ostream.print("\n" + nom + ":Voulez-vous demarrer un nouvelle partie: (o/n)");
            String ok = istream.readLine();
            return (ok.equalsIgnoreCase("y")
                    || ok.equalsIgnoreCase("yes")
                    || ok.equalsIgnoreCase("o")
                    || ok.equalsIgnoreCase("oui"));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean demanderTerminerPartie() {
        try {
            ostream.print("\n" + nom + ":Voulez-vous terminer la partie: (o/n)");
            String ok = istream.readLine();
            return (ok.equalsIgnoreCase("y")
                    || ok.equalsIgnoreCase("yes")
                    || ok.equalsIgnoreCase("o")
                    || ok.equalsIgnoreCase("oui"));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public String getNom() {
        if (nom != null) {
            return nom;
        }

        try {
            ostream.print("\nVotre nom:");
            nom = istream.readLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return nom;
    }

    @Override
    public Card[] jouer(int i, Card carte) {
        String[] cs;
        String ligne;
        Card[] cartes;
        int u;

        try {
            ostream.print("\n" + nom + ":Quelles cartes voulez-vous jouees:");
            ostream.print("\nex: ms pour message -1 pour piocher 0 pour passer sinon 1,2, 3");
            ostream.print("\nVotre main:");
            for (int j = 0; j < handCards.size(); j++) {
                ostream.print(" " + (j + 1) + "=" + handCards.get(j));
            }
            ostream.print("\nVos cartes:");
            ligne = istream.readLine();
            if (ligne.equals("ms")) {
                ostream.print("\nVotre message:");
                ligne = istream.readLine();
                envoyerMessage(ligne);

                ostream.print("\nVotre main:");
                ligne = istream.readLine();
            }

            cs = ligne.split(",");
            cartes = new Card[cs.length];
            for (int j = 0; j < cs.length; j++) {
                u = Integer.parseInt(cs[j]);
                if (u == 0) {
                    passerTour();
                    return null;
                }
                if (u == -1) {
                    piocher();
                    return null;
                }
                u--;
                cartes[j] = handCards.get(u);
            }
            return cartes;

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setCouleur(String couleur) {
        super.setCouleur(couleur);

        //
        ostream.print("Couleur demandee: " + couleur);
    }

    @Override
    public void setMessage(String message) {
        super.setMessage(message);

        ostream.println(message);
    }

    @Override
    public void aDeposer(int id, Card[] cartes) {
        super.aDeposer(id, cartes);
        ostream.print("Table: ");
        if (tables.size() >= 4) {
            for (int i = tables.size() - 4; i < tables.size(); i++) {
                ostream.print(tables.get(i));
            }
        } else {
            for (int i = 0; i < tables.size(); i++) {
                ostream.print(tables.get(i));
            }
        }
        ostream.println();
    }

    @Override
    public void setCartesMain(Card[] cartes) {
        super.setCartesMain(cartes);
        ostream.print("\nVotre main:");
        for (int j = 0; j < handCards.size(); j++) {
            ostream.print(" " + (j + 1) + "=" + handCards.get(j));
        }
        ostream.println();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        byte[] addr = {127, 0, 0, 1};
        int port = 7000;

        try {
            JoueurClient jc1 = new JoueurClient(addr, port);
            JoueurConsole jcol1 = new JoueurConsole(jc1);
            jcol1.setNom("Console");

            Thread t1 = new Thread(jc1);

            t1.start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
