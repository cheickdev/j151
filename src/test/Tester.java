package test;

import graphic.JoueurClient;
import graphic.JoueurOrdinateur;
import graphic.MaFenetre;

import java.io.IOException;

import serveurJeu.ServeurJeu;

public class Tester {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		byte[] addr = { 127, 0, 0, 1};
        int port = 7000;
        ServeurJeu serveurJeu = new ServeurJeu(port);
        Thread ts = new Thread(serveurJeu);
        ts.start();
        JoueurClient jc1 = new JoueurClient(addr, port);
        JoueurClient jc2 = new JoueurClient(addr, port);
        JoueurOrdinateur jo1 = new JoueurOrdinateur(jc1);
        JoueurOrdinateur jo2 = new JoueurOrdinateur(jc2);
        MaFenetre mf1 = new MaFenetre(jo1, "JO1");
        MaFenetre mf2 = new MaFenetre(jo2, "JO2");
        jo1.setMaFenetre(mf1);
        jo2.setMaFenetre(mf2);
        jo1.getMaFenetre().setVisible(true);
        jo2.getMaFenetre().setVisible(true);
	}

}
